---
title: ""
date: 2018-08-02T08:41:48+08:00
lastmod: 2018-08-02T08:48+08:00
draft: false
tags: ["Coral-Gables", "Events"]
categories: ["Events"]
author: "Futuresarcade"
autoCollapseToc: true
contentCopyright: ''
---
![Imgur](https://i.imgur.com/sBfDw0V.jpg?1)

## MIAMI SPICE

>**Date:**
>August 1st - September 30th
>[**Participating Restaurants**](http://www.miamiandbeaches.lat/special-offers/monthly-deals/miami-spice-month/miami-spice-month-search-results/?progcode=spice&sort=rand&loc=coralgab)

Miami Spice is a mouth-watering restaurant promotion showcasing the very best of Miami cuisine. Participating restaurants offer three-course meals featuring signature dishes created by world-renowned chefs at reduced prices: Lunch/Brunch $23 and Dinner $39.

----------
## CORAL GABLES GALLERY NIGHT

> **Date:**
>Friday 8-3 6:30 pm – 9:00 pm
>**Location:**
>Various galleries
>Coral Gables, Florida
>**Cost:** Free
>[Download the Gallery Night map](http://www.coralgables.com/Modules/ShowDocument.aspx?documentID=14102)

Held the first Friday of the month, Gallery Nights showcase the various works from European and Latin American art masters to outstanding contemporary artists. Be on the lookout for the orange paintbrush on the side of the Trolley and on special Trolley stop signs to find a dedicated Gallery Night Trolley that will loop between Coral Gables galleries and art venues participating on Gallery Night.

-------------------------

![Imgur](https://i.imgur.com/xZbnM4I.jpg?1)

## MERCEDES-BENZ - Gallery Night Live!

> **Date:**
>Friday 8-3, 6:30 pm-10 pm
>**Location:**
>Coral Gables Museum
>285 Aragon Avenue
>Coral Gables, FL 33134
>**Cost:** Free
[MAP](https://www.google.com/maps/place/Coral+Gables+Museum/@25.7505925,-80.2625289,17z/data=!3m1!4b1!4m5!3m4!1s0x88d9b79a1b3ffaa5:0x9aad47c710d60050!8m2!3d25.7505925!4d-80.2603402)

This event happens every first Friday of the month in conjunction with Coral Gables Gallery Night. Gallery Night Live, sponsored by Mercedes-Benz of Coral Gables, for the public opening of the exhibition, Sacred Ground: The Rise, Fall and Revival of Lincoln Memorial Park Cemetery in Gallery 109 and the Anthony R. Abraham Family Gallery.
[More](http://coralgablesmuseum.org/event/mercedes-benz-of-coral-gables-gallery-night-live-7/)

------------------------

![Imgur](https://i.imgur.com/T8UbtR2.jpg?1)

## FLOAT - A twilight parade

>**Date:**
>Saturday August 4th, 8:00 - 9:30 pm
>**Location:**
>Vizcaya Museum & Gardens
>3251 S. Miami Ave.
>Miami
>[Map](https://goo.gl/maps/k8SGD9nbEDz)
[vizcaya.org](https://vizcayatickets.org/)
Cost: $10

The float will explore James Deering’s romance with the sea, and his concept of Vizcaya as a nautical fantasy. Designed as a cumulative procession that connects both sides of Vizcaya, as well as incorporating lanterns from Whirl and Bloom, it will start at twilight in the Village and, like a stream, make its way to the waterfront, growing as it encounters and absorbs lantern into its flow.

--------

![Imgur](https://i.imgur.com/BhcHULr.jpg?1)
## FIRST FRIDAY ICA (Institute of Contemporary Art)

> **Date:**
>Friday 8/2, 6:00 pm-10 pm
>**Location:**
>ICA MIAMI
>61 NE 41st Street
>Miami, FL 33137
>**Cost:** Free
[MAP](https://goo.gl/maps/JjDN2U1GH7N2)

ICA Miami, celebrates all mediums of art, from film screenings to exhibition tours to public art institutions. First Fridays are always free and presented in part with the folks at Mini USA.

----------------------

![Imgur](https://i.imgur.com/ADp0RWq.png?1)

## BOOKS AND BOOKS

>**Date:**
>Saturday 6/23 7:00 p.m.
>**Location:**
>Books & Books in Coral Gables,
>265 Aragon Ave
>Coral Gables, FL 33134
>[Map](https://goo.gl/maps/2XgRch3XXYS2)

The Café’s Wine Bar, located at the entrance to the courtyard, features craft beer and wines from around the world offered by the glass or by the bottle. A free, live music series showcases musicians of all types and genres, including Jazz, African, Latin, Caribbean and World Beat. Every Friday and Saturday beginning at 7:00pm.

-----------------------

![Imgur](https://i.imgur.com/VmWfshy.jpg?1)

## ALMODOVAR THE TOP TEN

>**Date:**
>Friday 7-27 through August 9
[**Show Times**](http://www.gablescinema.com/programs/almodovar-the-top-ten/)
>**Location:**
>Gables Cinema
>260 Aragon Ave
>Coral Gables, FL 33134-5008
>**Cost:** $11.75
>[Info](http://www.gablescinema.com/programs/almodovar-the-top-ten/)
>[Map](https://www.google.com/maps/place/Coral+Gables+Art+Cinema/@25.7503268,-80.2620697,17z/data=!3m1!4b1!4m5!3m4!1s0x88d9b799f8f757db:0x239eb89da7691c54!8m2!3d25.750322!4d-80.259881)

 Our series surveys ten classics made over the course of 25 years, and feature not only his famous ensemble, fondly referred to as chicos y chicas Almodóvar, but also actors like Antonio Banderas, Penélope Cruz and Javier Bardem who went on to international stardom.

--------------
![Imgur](https://i.imgur.com/HMTbscX.jpg?1)
## FUACATA

>**Date:**
>Thursday August 2nd - Sunday, August 19
>[**INFO AND TICKETS**](http://www.arshtcenter.org/en/Tickets/Calendar/2017-2018-Season/Summer-Shows/Fuacata/)
>**Location**
>Adrienne Arsht Center for the Performing Arts
>1300 Biscayne Blvd.
>Miami
>**Cost**  $50
>[MAP](https://goo.gl/maps/srSLxLuZerJ2)

This one-woman tour-de-force performance brings more than 20 Latina women to glorious life on stage by weaving together tales of love, marriage, immigration, and identity through eclectic stories and whimsical song!

------------------

![Imgur](https://i.imgur.com/b2jqba8.png?2)

## BALLET FESTIVAL

>**Date:**
> Saturday July 28th - August 19th
>**Location:**
>Miami, Miami Beach, Broward
>[**Tickets**](https://www.internationalballetfestival.org/schedule-tickets)

The world-acclaimed International Ballet Festival of Miami celebrates its 23rd edition with a series of galas featuring leading dance companies from different parts of the world, and the presence of award-winning dancers. This Festival represents the most ambitious ballet project ever conceived in South Florida, the magnitude of its program, along with the prestige of the participating companies and the renowned principal ballet stars, have captured the attention of the most demanding audiences.

------------

![Imgur](https://i.imgur.com/7jes8MJ.jpg?1)

## FARMERS MARKET

>**Date:**
>Sunday Year Round 11:00 am – 5:00 pm
>**Location:**
>Shops at Merrick Village
>358 San Lorenzo Ave
>Coral Gables, FL 33146
>[MAP](https://goo.gl/maps/VAzZoWGLfCs)

The Farmers’ Market is located between Tourneau and Nordstrom and takes place from 11 AM – 5 PM every Sunday, year round. We look forward to seeing you at the market.

-------------------

![FEATURED LISTING](https://www.featuredlisting.forsale/images/4livingformal.png)

## FEATURED LISTING FOR SALE:

### 435 TIVOLI, CORAL GABLES, FL 33143

This spectacular home was redesigned and upgraded to the maximum current standards, it is evident the moment you step up through the front door you will encounter the epitome of modernism, functional design, simple lines, double ceilings, panoramic impact windows that bring the outdoors inside. No expense spared in the renovation.

[![Imgur](https://i.imgur.com/nW3VryC.png?1)](https://www.youtube.com/watch?v=1Fgf9IaaBiU&feature=youtu.be")

[WEBSITE](https://www.featuredlisting.forsale/)
