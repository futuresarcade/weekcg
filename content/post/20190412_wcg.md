---
title: "20190412_wcg"
date: 2019-04-10T15:13:27-04:00
draft: false
tags: ["Coral-Gables","Events"]
categories: ["Events"]
author: "Futuresarcade"
autoCollapseToc: false
contentCopyright: ''
---

[![Banner](https://www.weekincoralgables.com/DianaBanner2.png)](http://yourhomeincoralgables.com/)

-------------------

![Miami Design District](https://www.miamidesigndistrict.net/images/events/square/1464_gloria-gaynor-live-in-the-miami-design-district_572604.jpg =300x)

## GLORIA GAYNOR LIVE
IN THE MIAMI DESIGN DISTRICT

>**Date:** \
04 -12 -2019, 6 p.m. - 9 p.m. \
>**Location:** \
Palm Court, \
140 NE 39th Street, \
Miami, FL 33137
[MAP](https://goo.gl/maps/Xi5J5HbWxgJ2)


The Miami Design District Performance Series returns to Palm Court with its FREE Friday evening ensemble of live musical performances produced by 19-time Grammy© Award winning producer Emilio Estefan in collaboration with the Miami Symphony Orchestra presented by the Knight Foundation.

[READ MORE](https://www.miamidesigndistrict.net/event/1464/gloria-gaynor-live-in-the-miami-design-district/)

---------------------

![BIG BUNNY INVASION](https://www.pinecrestgardens.org/Home/ShowPublishedImage/5905/636688018206300000 =300x)

## BIG BUNNY INVASION

>**Date:** \
April 5th - April 20th, \
>**Location:** Pinecrest Gardens, \
11000 Red Rd, \
Pinecrest, FL 33156, \
[MAP](https://goo.gl/maps/dzVAfyyVYxp), \ **Cost:**$5

This April, Pinecrest Gardens is tossing out the traditional “hunt” and replacing it with relaxineg weekend evenings where both children and adults can play. Edgy and ephemeral, Big Bunny Invasion is a public light-art installation that blends fine art and community to ignite imaginations while providing the perfect setting for families to unwind, create and learn.

[READ MORE](https://www.pinecrestgardens.org/entertainment/events-festivals/eggstravaganza)

-------------------------

![MIAMI POETRY](https://static1.squarespace.com/static/51acf210e4b09715f92c4566/t/5c8c497a8165f563526c15a1/1552697731584/edited.jpg?format=1500w =300x)

## O, MIAMI POETRY FESTIVAL

>**Date:** \
APRIL 1 - 30 \
>**Location:** \
Throughout Miami

April is your chance to encounter poetry in lots of different formats throughout Miami. Each spring, the O, Miami Poetry Festival attempts to deliver a poem to every single person in Miami-Dade County. Sponsored by the John S. and James L. Knight Foundation, the festival features more than 40 free events and 20 projects throughout the city. Most events are free.

[READ MORE](http://www.omiami.org/festival/)

-------------

![Children Concerts](https://www.pinecrestgardens.org/home/showimage?id=6963 =300x)

## Fun Fables and Other Tales Told through Music

>**Date:** \
Sunday, April 14, 2019 at 3:00 p.m. \
>**Location:** \
Pinecrest Gardens, \
11000 Red Rd, \
Pinecrest, FL 33156, \
[MAP](https://goo.gl/maps/dzVAfyyVYxp), \
>**Cost:** 18 \
[Buy tickets](https://www.orchestramiami.org/fun-fables)

The Frog Prince, How Brer Raccoon Outsmarted the Frog, and Three Fun Fables (The Fox and the Crow, The Dog and His Reflection & the Tortoise & the Hare) performed by musicians from Orchestra Miami.

[READ MORE](https://www.pinecrestgardens.org/entertainment/concerts-theater-dance/family-fun-concert-series)

-------------------


![Out to Brunch](https://image-ticketfly.imgix.net/00/03/10/52/66-og.jpg =300x)

## New Times Out to Brunch

>**Date:** \
April 13, 2019, 11:00 AM to 03:00 PM \
**Location:** \
Jungle Plaza \
3801 NE 1st Ave \
Miami, FL 33137 \
[MAP](https://goo.gl/maps/iz56No9mcxz) \
>**Cost:***$40+

If you're a brunch lover, get your squad together for New Times' Out to Brunch. Soho Studios will transform into the biggest brunch party Miami has ever seen. Enjoy brunch items from South Florida's most popular eateries, as well as, mimosas, beermosas, a bloody mary bar, and a host of brunch cocktails. A portion of ticket proceeds will benefit Special Olympics Florida.

[READ MORE](https://www.ticketfly.com/purchase/event/1804419?utm_source=MNT&utm_medium=Microsite&utm_campaign=Onsale)

---------------------
![Earth Day](http://coralgablesmuseum.org/wp-content/uploads/2019/03/whatpercento-845x321.jpg =300x)
## Family Day on Aragon:
#### The Gables Canopy: Celebrating Earth Day

>**Date:** \
APRIL 13 @ 2:00 PM - 5:00 PM \
> **Location:** \
Coral Gables Museum, \
285 Aragon Avenue, \
Coral Gables, FL 33134, \
[MAP](https://goo.gl/maps/XknzN26PMAK2), \
 **Cost:** Free

Spring into a fun filled celebration of the beautiful trees that make up the Coral Gables Canopy.  We will have fun earth day crafts for kids, Penelope Robin will be performing, as will the Miami Symphony Orchestra.  You won’t be“leaf” how much fun you’re gonna have.  This event is FREE and OPEN to the public.

[READ MORE](http://coralgablesmuseum.org/event/family-day-on-aragon-the-gables-canopy-celebrating-earth-day/)

#### Storytime & Craft
>**Date:** \
April 13 @ 10:30 am \
>**Location:**
Books & Books in Coral Gables, \
265 Aragon Ave \
Coral Gables, FL 33134

The Gables Canopy: Celebrating Earth Day!  Children of all ages are invited with their parents to spring into a fun filled celebration of the beautiful trees that make up the Coral Gables Canopy.  We will have fun Earth day crafts for kids and a themed story time. You won’t be “leaf” how much fun you’re gonna have. This event is FREE and OPEN to the public. See you there! Coral Gables at 10:30am   Every Saturday in  Coral Gables at 10:30am & Suniland Shops at…

[READ MORE](https://booksandbooks.com/event/storytime-craft-2019-04-13/)

---------------------

![Bernacle Park](https://thebarnacle.org/wp-content/uploads/2019/01/row-about-full.jpg =300x)

## Under the Moonlight Concert

>**Date:** \
April 13, 2019, 07:00 PM to 09:00 PM \
>**Location:** \
The Barnacle Historic State Park \
3485 Main Highway \
Coconut Grove, FL 33133 \
[MAP](https://goo.gl/maps/XbztiuU2dTo) \
>**Cost:** $10 Adults.

Bring a picnic, blanket and lawn chairs, and enjoy an evening of folk Americana music under the moonlight. Gates open at 6 pm. No pets, please.

[READ MORE](https://thebarnacle.org/)

----------------

![Imgur](https://i.imgur.com/7jes8MJ.jpg?3 =300x)


## FARMERS MARKET

>**Date:** \
Sunday Year Round 11:00 am – 5:00 pm \
>**Location:** \
Shops at Merrick Village \
358 San Lorenzo Ave \
Coral Gables, FL 33146 \
[MAP](https://goo.gl/maps/VAzZoWGLfCs)

The Farmers’ Market is located between Tourneau and Nordstrom and takes place from 11 AM – 5 PM every Sunday, year round. We look forward to seeing you at the market.

[READ MORE](https://www.shopsatmerrickpark.com/en/events/the-farmers-market-at-merrick-park-18504.html)

-------------

![614 San Antonio Av](https://bt-photos.global.ssl.fastly.net/miami/orig_boomver_2_A10653576-1.jpg)

## FEATURED LISTING FOR RENT

>**Location:** \
614 San Antonio \
Coral Gables, FL 33146 \
[MAP](https://goo.gl/maps/xNG8cQnxvhS2)

Fresh - Bright - Light - Clean modern feel.

[READ MORE](https://www.ewm.com/homes/614-San-Antonio-Av/Coral-Gables/FL/33146/93537558/)
