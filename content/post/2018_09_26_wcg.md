---
title: ""
date: 2018-09-27T11:08:48+08:00
lastmod: 2018-09-27T11:08:49+08:00
draft: false
tags: ["Coral-Gables","Events"]
categories: ["Events"]
author: "Futuresarcade"
autoCollapseToc: true
contentCopyright: ''
---

[![Banner](https://www.weekincoralgables.com/DianaBanner.png)](http://yourhomeincoralgables.com/)

------------------------

![Imgur](https://i.imgur.com/sBfDw0V.jpg?1)

## MIAMI SPICE

>**Date:**
>August 1st - September 30th
>[**Participating Restaurants**](http://www.miamiandbeaches.lat/special-offers/monthly-deals/miami-spice-month/miami-spice-month-search-results/?progcode=spice&sort=rand&loc=coralgab)

Miami Spice is a mouth-watering restaurant promotion showcasing the very best of Miami cuisine. Participating restaurants offer three-course meals featuring signature dishes created by world-renowned chefs at reduced prices: Lunch/Brunch $23 and Dinner $39.

[READ MORE](http://www.miamiandbeaches.lat/special-offers/monthly-deals/miami-spice-month)

---------------------------
![Imgur](https://i.imgur.com/Nkt3ABo.jpg?1)

## CUSTOMER APPRECIATION DAY

>**Date:**
Saturday at 10 AM – 8 PM
>**Location:**
Downtown Coral Gables & Miracle Mile
220 Miracle Mile
Coral Gables, Florida 33134

On Saturday, September 29, explore Downtown Coral Gables shops and restaurants during Customer Appreciation Day! As a token of appreciation, participating retail and specialty stores will provide special offers and free goodies throughout the day. See below for a full list of participating businesses:

[READ MORE](http://www.shopcoralgables.com/feature/downtown-coral-gables-customer-appreciation-day/)

----------------------

![Imgur](https://i.imgur.com/4Ue7kJj.jpg?1)

## OKTOBER FEST - CORAL GABLES

>**Date:**
Sept. 27-30
6-10 p.m. Thursday
6-11 p.m. Friday
4- 11 p.m. Saturday
2-8 p.m. Sunday
>**Location:**
Bierhaus Plaza,
60 Merrick Way
Coral Gables
>**Cost:** Free

Join your friends from Fritz & Franz Bierhaus for beer, food and more. Fritz & Franz serves up its Oktoberfest menu from Sept. 27-Oct. 28.

[READ MORE](https://www.miami.com/things-to-do-in-miami/raise-a-glass-here-are-the-best-oktoberfest-2018-events-in-miami-196994/)

--------------------

![Imgur](https://i.imgur.com/ynR218h.jpg?1)

## VIERNES CULTURALES /CULTURAL FRIDAY

>**Date:**
>Friday 9-28 7:00 -11:00 p.m.
>**Location:**
>8th Street between 13th – 17th  Avenues
>Miami, FL 33174
>Map
>**Cost:** Free
>http://viernesculturales.org/mainvc/

Viernes Culturales features music and works by local artists and artisans in and around Domino Plaza on Southwest 8th Street and 5th Avenue, and behold contemporary and historic artworks at many fine art galleries along Calle Ocho. Savor a delicious dinner at one of the local restaurants, boasting cuisine from all over the world. Come dance, eat, shop and enjoy a delightful evening in Little Havana.

[READ MORE](http://viernesculturales.org/mainvc/)

------------------------

![Imgur](https://i.imgur.com/CUOPCqK.png?1)

## ORCHID HOLIDAY

>**Date:**
Saturday 9/29 and Sunday 9/30
from 10:00 am - 5:00 pm
>**Location
Watsco Center University of Miami
1245 Dauer Drive, Coral Gables, 33146.
>**Cost** $10
[MAP](https://goo.gl/maps/uZzTYQ7W54D2)

The 72nd Miami International Orchid Show sponsored by the South Florida Orchid Society Featuring Internationally Acclaimed Orchid Growers From around the World and the U.S. Hundreds of Orchids for Sale, Tabletop Orchid Displays. How to Grow Lectures and Demonstrations will be given on Saturday. AOS Judged Show

[READ MORE](https://www.sforchid.com/)

-----------------------

![National Theather Live](http://www.gablescinema.com/media/filmassets/NTL_2018_-_Julie_-_INT_Listing_Image_-_Portrait.jpg.500x715_q85_crop-smart.jpg)

## NATIONAL THEATER LIVE –JULIE

>Date:
Friday 9/28 through Sunday 9/30  @ 12:00 pm
>Location:
Gables Cinema
260 Aragon Ave
Coral Gables, FL 33134-5008
Cost: $20
>[Map](https://www.google.com/maps/place/Coral+Gables+Art+Cinema/@25.7503268,-80.2620697,17z/data=!3m1!4b1!4m5!3m4!1s0x88d9b799f8f757db:0x239eb89da7691c54!8m2!3d25.750322!4d-80.259881)

[READ MORE](http://www.gablescinema.com/events/julie/)

-----------------

![Imgur](https://i.imgur.com/ADp0RWq.png?1)

## BOOKS AND BOOKS

>**Date:**
>Friday and Saturday 7:00 p.m.
>**Location:**
>Books & Books in Coral Gables,
>265 Aragon Ave
>Coral Gables, FL 33134
>[Map](https://goo.gl/maps/2XgRch3XXYS2)

The Café’s Wine Bar, located at the entrance to the courtyard, features craft beer and wines from around the world offered by the glass or by the bottle. A free, live music series showcases musicians of all types and genres, including Jazz, African, Latin, Caribbean and World Beat. Every Friday and Saturday beginning at 7:00pm.

[READ MORE](http://www.thecafeatbooksandbooks.com/)

--------------------------

![Imgur](https://i.imgur.com/rfOmd7P.jpg?1)

## INSPIRED BY CORAL GABLES

>**Date:**
Friday, September 28th, 2018 - 6 pm
Lecture by Prof. Teofilo Victoria 6:30 pm
>**Location:**
Ninoska Huerta Gallery
290 Aragon Ave
Coral Gables FL 33134
RSVP 305-588-1231

"Inspired by Coral Gables" presents the watercolors and drawings of the artists: Carlos Barbon, Victor Deupi, Brian Lemmerman, Thomas Spain, and original drawings of Denman Fink and The Biltmore Hotel.

[READ MORE](http://www.ninoskahuertagallery.com/)

------------------

![Imgur](https://i.imgur.com/7jes8MJ.jpg?3)

## FARMERS MARKET

>**Date:**
>Sunday Year Round 11:00 am – 5:00 pm
>**Location:**
>Shops at Merrick Village
>358 San Lorenzo Ave
>Coral Gables, FL 33146
>[MAP](https://goo.gl/maps/VAzZoWGLfCs)

The Farmers’ Market is located between Tourneau and Nordstrom and takes place from 11 AM – 5 PM every Sunday, year round. We look forward to seeing you at the market.

[READ MORE](https://www.shopsatmerrickpark.com/en/events/the-farmers-market-at-merrick-park-18504.html)

-------------------

![OpenHouse](https://www.weekincoralgables.com/OpenHouse0930.png)

### FEATURED LISTING- 435 TIVOLI, CORAL GABLES, FL 33143

This spectacular home was redesigned and upgraded to the maximum current standards, it is evident the moment you step up through the front door you will encounter the epitome of modernism, functional design, simple lines, double ceilings, panoramic impact windows that bring the outdoors inside. No expense spared in the renovation.

[READ MORE](https://www.435tivolicoralgables.com/)

[![Imgur](https://i.imgur.com/nW3VryC.png?1)](https://www.youtube.com/watch?v=1Fgf9IaaBiU&feature=youtu.be")

---------------
