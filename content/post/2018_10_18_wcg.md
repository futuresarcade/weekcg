---
title: ""
date: 2018-10-18T11:08:48+08:00
lastmod: 2018-10-18T11:08:49+08:00
draft: false
tags: ["Coral-Gables","Events"]
categories: ["Events"]
author: "Futuresarcade"
autoCollapseToc: true
contentCopyright: ''
---

[![Banner](https://www.weekincoralgables.com/DianaBanner2.png)](http://yourhomeincoralgables.com/)

--------------------

![Imgur](https://i.imgur.com/08U6HT0.jpg?1)

## MIAMI SYMPHONY ORCHESTRA LIVE

>**Date:**
Fridays 10-19 6:00 pm – 9:00 pm
**Location:**
Palm Court - Miami Design District
140 NE 39 St.
Miami, FL 33137
(305) 722-7100
**Cost:** Free
[MAP](https://goo.gl/maps/8nurG3FUyrK2)

On October 19, enjoy a performance by the Miami Symphony Orchestra (MISO). The Miami Symphony Orchestra (MISO), now in its historic 30th season is Miami's hometown professional symphony and a valuable contributor to Miami's cultural fabric. Led by MISO Music Director & Conductor Maestro Eduardo Marturet, it represents the exceptional talent and diversity that mirrors South Florida's international and multicultural richness.

[READ MORE](https://www.miamidesigndistrict.net/event/1259/miami-symphony-orchestra-live-in-the-design-district/)

----------------------

![Imgur](https://i.imgur.com/GwRClVG.gif?1)

## OKTOBER FEST

>**Date:**
Friday October 19, 6:00 P.M.
Saturday October 20, 1:00 P.M.
Sunday October 21, Noon
**Location:**
German American Social Club
11919 SW 56th St. Miami
[MAP](https://goo.gl/maps/5fWKj5y83H22)
**Cost**: $10

Want to experience the real Oktoberfest deal? Join the German American Social Club and celebrate the best of German culture. This family-friendly event features bounce houses, games, rides and magic shows for the kids.

[READ MORE](http://gascmiami.org/oktoberfestmiami)

----------------


![Imgur](https://i.imgur.com/ADp0RWq.png?1)

## BOOKS AND BOOKS

>**Date:**
>Friday and Saturday 7:00 p.m.
>**Location:**
>Books & Books in Coral Gables,
>265 Aragon Ave
>Coral Gables, FL 33134
>[Map](https://goo.gl/maps/2XgRch3XXYS2)

The Café’s Wine Bar, located at the entrance to the courtyard, features craft beer and wines from around the world offered by the glass or by the bottle. A free, live music series showcases musicians of all types and genres, including Jazz, African, Latin, Caribbean and World Beat. Every Friday and Saturday beginning at 7:00pm.

[READ MORE](http://www.thecafeatbooksandbooks.com/events.html)

--------------------------

![Imgur](https://i.imgur.com/Dh0MluD.jpg?1)

## Coral Gables Hispanic Cultural Festival

>**Date:**
Saturday October 20th
12:00 pm - 9:00 pm
Sunday October 21st
12:00 pm - 8:00 pm
**Location:**
Biltmore Way & LeJune Rd.
Coral Gables, FL 33134
[MAP](https://goo.gl/maps/NacZbyHFJCt)

Come out and enjoy a weekend filled with art, culture, and live dances/folkloric performances and musical acts. This family friendly event has something for everyone.

[READ MORE](https://www.gableshispanicfestival.com/)

--------------------

![Imgur](https://i.imgur.com/ZQqoyeN.jpg?1)

## HAVANA MUSIC HALL

>**Date:**
>October 10 - November 18th, 2018
>[Tickets](https://tickets.actorsplayhouse.org/TheatreManager/1/online)
>**Location:**
>Actor’s Play House
>280 Miracle Mile, Coral Gables, FL 33134
>www.actorsplayhouse.org
>**Cost:** $40
[MAP](https://goo.gl/maps/qLsjCYiSAFp)

This brand new musical was selected specifically by our artistic team to develop in Miami on its way to Broadway. An exciting world premiere, Havana Music Hall reflects the pulse of our community with its irresistible musical beat and heartwarming story centering on Cuban musicians Rolando and Ramona Calderon, and their lives in Cuba from 1958 to the present. Just as their local Havana band is about to get its big break, the Revolution tears apart the only world the duo have ever known. This moving and relevant new musical explores the challenges and joys of one family’s 50-year odyssey for survival, presenting a journey which immigrants of all cultures from around the world have traveled for generations.

[READ MORE](https://www.actorsplayhouse.org/mainstage.htm)

---------------------


![Imgur](https://i.imgur.com/ZMfrOS6.png?1)

## ADMISSIONS

>**Date:**
Saturday October 13 - November 11
[Tickets](https://web.ovationtix.com/trs/cal/473)
>**Location:**
Gables Stage
1200 Anastasia Avenue
Coral Gables, Florida 33134
[Map](https://goo.gl/maps/5hx1vvHFhQy)
**Cost:** $55

The head of the admissions department at a New England prep school is fighting to diversify the student body. With her husband, the school’s Headmaster, they’ve brought a stodgy institution into the 21st century. When their only son sets his sights on an Ivy League university, personal ambition collides with progressive values! A play that explodes the ideals and contradictions of liberal white America. By the author of BAD JEWS.
“ASTONISHING AND DARING! An extraordinarily useful and excruciating satire – of the left, by the left, for the left – for today!” – New York Times

[READ MORE](http://www.gablestage.org/2017-2018-season/)

-------------------

![Imgur](https://i.imgur.com/FOZ4g8v.png?1)

## FOREVER CELIA

>**Date:**
October 20th - March 31st
Tuesday to Saturday 10 A.M.- 4:00 P.M
>**Location:**
Cuban Museum
1200 Coral Way
Miami, FL 33145
[MAP](https://goo.gl/maps/jELy1RKwh9C2)

Dance through the decades to the sound of Celia Cruz’s inimitable voice, one immortal album at a time. Opening October 20th, 2018 at Miami’s new American Museum of the Cuban Diaspora, the exhibit offers visitors a comprehensive look into the life of the uncontested Queen of Salsa, Celia Cruz, in the context of her journey as a Cuban exile, fashion trailblazer, and music icon. On exhibit through March 31st, 2019, the show features items from Celia’s trademark extravagant wardrobe, and never-before-seen artifacts culled from her personal collection. It offers the public unprecedented insight into both the private life, and public persona of the woman who will remain in our hearts.

[READ MORE](http://www.thecuban.org/)

------------------------
![Imgur](https://i.imgur.com/exmeqtN.jpg?1)
###### Dimensions Dance Theatre of Miam, dancer Mayrel Martinez in Imagined Notions, photographer Simon Soong

## DANIEL LEWIS DANCE SAMPLER
#### A preview of season to come

>**Date:**
Saturday October 20th, 3 and 8 P.M.
**Location:**
NEW WORLD SCHOOL OF ARTS
25 NE Second St, Eigth Floor
Miami 33132
**Cost:**$25
[MAP](https://goo.gl/maps/dXAQ4dUDGho)

The Sampler offers a taste of what each local troupe will be presenting in the coming season. “The mission of the Sampler is to showcase Miami’s established companies and choreographers,” says Hannah Baumgarten, co-director with Diego Salterini of Dance Now! Miami. The company has been presenting the Sampler since 2011, naming it the Daniel Lewis Miami Dance Sampler after its founder, choreographer and former Dean of Dance at the New World School of the Arts, Daniel Lewis.

[READ MORE](https://www.miamiherald.com/entertainment/performing-arts/article220110485.html)

----------------

![Imgur](https://i.imgur.com/APKX78C.gif?1)


## RANDY BRECKER WITH THE SOUTH FLORIDA JAZZ ORCHESTRA
#### South Motor Jazz Series

>**Date:**
Saturday October 20th, 8:00 P.M.
>**Location:**
PINECREST GARDENS
11000 Read Road, Pinecrest FL 33156
**Cost:**$40
[MAP](https://goo.gl/maps/dPvi1Mm18z42)

Six Time Grammy Award Winner
Randy Brecker
and the South Florida Jazz Orchestra
Saturday, October 20, at 8:00 p.m.
Jazz trumpeter and composer Randy Brecker has helped shape the sound of jazz, R&B and rock for more than four decades. His trumpet and flugelhorn performances have graced hundreds of albums by a wide range of artists including James Taylor, Bruce Springsteen, Frank Sinatra, Steely Dan and Frank Zappa. In 1967, Randy ventured into jazz-rock with the band Blood, Sweat and Tears and after one album left to join the Horace Silver Quintet.

[READ MORE](https://www.pinecrestgardens.org/entertainment/concerts-theater-dance/south-motors-jazz-series)

------------------------

![Imgur](https://i.imgur.com/p1Ph0rj.jpg?1)

## FEATURED LISTING FOR RENT $2850
### Furnished or Unfurnished

### 2030 S. Douglas Rd. #423 Coral Gables

Two bedrooms, two bathrooms, den and big terrace.  Its central location & close proximity to Miracle Mile allows you to walk or take the trolley to a great selection of the best restaurants, café’s, shops, fine art galleries & more, close to public transportation.


[READ MORE](http://yourhomeincoralgables.com/listings/)

## EWM #1 BROKERAGE IN CORAL GABLES

![Imgur](https://i.imgur.com/KMHHgA0.jpg?1)

## Call Us For your Real Estate needs
### Diana Ordonez Tel: 305-510-4698
