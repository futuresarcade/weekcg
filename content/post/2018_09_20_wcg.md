---
title: ""
date: 2018-09-20T11:08:48+08:00
lastmod: 2018-09-20T11:08:49+08:00
draft: false
tags: ["Coral-Gables","Events"]
categories: ["Events"]
author: "Futuresarcade"
autoCollapseToc: true
contentCopyright: ''
---

[![Banner](https://www.weekincoralgables.com/DianaBanner.png)](http://yourhomeincoralgables.com/)

------------------

![Imgur](https://i.imgur.com/sBfDw0V.jpg?1)

## MIAMI SPICE

>**Date:**
>August 1st - September 30th
>[**Participating Restaurants**](http://www.miamiandbeaches.lat/special-offers/monthly-deals/miami-spice-month/miami-spice-month-search-results/?progcode=spice&sort=rand&loc=coralgab)

Miami Spice is a mouth-watering restaurant promotion showcasing the very best of Miami cuisine. Participating restaurants offer three-course meals featuring signature dishes created by world-renowned chefs at reduced prices: Lunch/Brunch $23 and Dinner $39.

[READ MORE](http://www.miamitemptations.com/miami-spice-month)

--------------------------------------

![Imgur](https://i.imgur.com/ADp0RWq.png?1)

## LIVE MUSIC IN THE COURTYARD

>**Date:**
>Friday and Saturday 7:00 p.m.
>**Location:**
>Books & Books in Coral Gables,
>265 Aragon Ave
>Coral Gables, FL 33134
>[Map](https://goo.gl/maps/2XgRch3XXYS2)

The Café’s Wine Bar, located at the entrance to the courtyard, features craft beer and wines from around the world offered by the glass or by the bottle. A free, live music series showcases musicians of all types and genres, including Jazz, African, Latin, Caribbean and World Beat. Every Friday and Saturday beginning at 7:00pm.

[READ MORE](http://www.thecafeatbooksandbooks.com/coral-gables.html)

--------------------

![Imgur](https://i.imgur.com/s4ZQW5C.jpg?1)

## A TOWN CALLED PANICK : DOUBLE FUN

>**Date:**
>Saturday 9-22, 10:00 am
>**Location:**
>Gables Cinema
>260 Aragon Ave
>Coral Gables, FL 33134-5008
>**Cost:** Free
>[Info](http://www.gablescinema.com/events/a-town-called-panic-fun/)
>[Map](https://www.google.com/maps/place/Coral+Gables+Art+Cinema/@25.7503268,-80.2620697,17z/data=!3m1!4b1!4m5!3m4!1s0x88d9b799f8f757db:0x239eb89da7691c54!8m2!3d25.750322!4d-80.259881)

Take a trip to A Town Called Panic, featuring two award-winning shorts from the directors of the Academy Award-nominated Ernest & Celestine! With disarming wit and hilarious visuals, A Town Called Panic: Double Fun revels in the simple setup of three plasticine toys sent on increasingly surreal and freewheeling adventures.

[READ MORE](http://www.gablescinema.com/events/a-town-called-panic-fun)

--------------------------
![Imgur](https://i.imgur.com/U8aMvEA.jpg?1)

## DINING WITH ETIKAT

>**Date:**
>Sat, September 22, 10:30 AM
>**Location:**
>Books & Books in Coral Gables,
>265 Aragon Ave
>Coral Gables, FL 33134
>[Map](https://goo.gl/maps/2XgRch3XXYS2)

An Adventure in Awesome Table Manners! is a one of a kind, interactive guide that teaches essential social skills and is loaded with practical tips, trivia and self-guiding visuals. Young readers enjoy learning etiquette with a feisty feline called, Etikat. The journey towards developing savvy children who understand that manners are both relevant and necessary for ultimate success is one of great fun. Scenarios where dining faux pas take place are those that everyone can relate to while laughing at Etikat’s silly…

[READ MORE](https://booksandbooks.com/events)

----------------------------

![Imgur](https://i.imgur.com/mvA8KzF.jpg?1)

## BUTTERFLY WALK

>**Date:**
>Saturday September 22, 11:00 am – 12:00 pm
>**Location:**
>Deering Estate
>16701 SW 72 Ave.
>Miami FL 33157 USA
>Phone: 305-235-1668
[MAP](https://goo.gl/maps/dgeiaTG5ZBF2)
>**Cost:** $12 $7 for children (4-14 years).

One of the most amazing insects for its beauty is the butterfly. Nature lovers and outdoor fans can take a guided tour of the grounds of Deering Estate, where there are many species of butterflies, which live in the garden. Be part of the Summer Butterfly Walk, an event for the whole family.

[READ MORE](http://www.deeringestate.org/butterfly-walks/)

-----------------

![Imgur](https://i.imgur.com/rhrA8cb.jpg?1)

## LIFE OVER BULLETS

>**Date:**
Sat, September 22, 2018
5:00 PM – 9:30 PM EDT
>**Location:**
Pinecrest Gardens
11000 Red Road
Pinecrest, FL 33156


The Miami Chapter of Students Demand Action for Gun Sense in America is hosting Life Over Bullets, a benefit concert to bring awareness on the very important issue of gun violence in our community. The proceeds will go to Guitars Over Guns and the RJT Foundation.
Come join us and watch a variety of student performers from across South Florida as we come together to make our voices heard. Food vendors will be on site.

[READ MORE](https://www.eventbrite.com/e/life-over-bullets-tickets-49676237965?aff=ebdssbdestsearch)

------------------------

![Imgur](https://i.imgur.com/rNHMuRx.jpg?1)

## NicaFest

>**Date**
Sunday September 23, 6:00 pm
>**Location**
Miami Dade County Auditorium
2901 W. Flagler St
Miami, FL
**Cost** $30

Concert and celebration of solidarity that supports refugees in Costa Rica and other places. Show your support while making friends and enjoying Nicaraguan music.

[READ MORE](https://www.facebook.com/NUEVA-N%C3%8DCARAGUA-401332147036104/?hc_ref=ARTU3y_wOJv9qRttmSuA_fc-gV2dasLDqm5if423ybsaaGcVcnhdLKWNZnIb4slqhHk&__xts__[0]=68.ARDlH7qX_wUIRQdt80aDlUX9RWe9MD-1E3Q4iBtAQ9Zj8A6qYEbWaOJXWr3VKapoZ00-rfqipLEXOnZeeLBu79aTQxQbLbK55XZKL2-Yw9YUx5W_f1sLJo9nKRuYj-b-nGKCQmk7rQr1BpLqIC507cIHEutXeFRm1xDr2HZCjLnOnhRdQPP1&__tn__=kC-R)

![Imgur](https://i.imgur.com/p1Ph0rj.jpg?1)

## FEATURED LISTING FOR RENT $2950

### 2030 S. Douglas Rd. #423 Coral Gables

Two bedrooms, two bathrooms, plus den furnished unit.  Its central location & close proximity to Miracle Mile allows you to walk or take the trolley to a great selection of the best restaurants, café’s, shops, fine art galleries & more, close to public transportation.

### Fully furnished

[READ MORE](http://yourhomeincoralgables.com/listings/)

## EWM #1 BROKERAGE IN CORAL GABLES

![Imgur](https://i.imgur.com/KMHHgA0.jpg?1)

## Call Us For your Real Estate needs
### Diana Ordonez Tel: 305-510-4698

------------------
