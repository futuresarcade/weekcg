---
title: "20191004_wcg"
date: 2019-10-03T11:23:55-04:00
draft: false
tags: ["Coral-Gables","Events"]
categories: ["Events"]
author: "Futuresarcade"
autoCollapseToc: false
contentCopyright: ''
---

<head>
    <style type="text/css">
      table {
        border:2px solid #552448;
        margin-top:10px;
        margin-bottom:50px;
      }
      article {
        clear: left;
      }
      img {
        width: 300px;
      }
    </style>
  </head>
<body>
<table width="700">
<tr valign="middle">
<td rowspan="2" width="220"  bgcolor= "#652f56" style="border-right: #552448 1px solid;">
<img src="https://i.imgur.com/r3dTAc6.png" title="source: imgur.com" width="250" alt="r3dTAc6.png">
</td>
<td rowspan="2" width="10">
</td>
<td>
<font style="color: #552448; font-family: Georgia; font-size: .95em"><strong>Diana Ordonez-Sanchez</strong></font><br />
<font style="color: #939598; font-family: Arial; font-size: .85em">Realtor Associate</font><br />
<font style="color: #939598; font-family: Arial; font-size: .85em">Chairman's Club</font><br />
<font style="color: #939598; font-family: Arial; font-size: .85em"><a href="tel:1-305-510-4698">1-305-510-4698</a> </font><br />
<font style="color: #939598; font-family: Arial; font-size: .85em">Berkshire Hathaway HomeServices EWM Realty | Realty Division</font><br />
</td>
</tr>
<tr>
<td height="40" valign="bottom">
<font style="color: #939598; font-family: Arial; font-size: .85em"><span style="color: #552448;">O:</span> 305-960-2400 |<span style="color: #552448;">E:</span> dianaiordonez@gmail.com | <a href="http://www.dianaordonez.com" style="text-decoration: none; color: #552448;">www.dianaordonez.com</a></font><br />
<font style="color: #939598; font-family: Arial; font-size: .85em">550 S. Dixie Hwy, Coral Gables, Florida 33146</font><br />
</td>
</tr>
</table>

</body>

---------------------------



![OKTOBERFEST CORAL GABLES](https://img1.wsimg.com/isteam/ip/c1ce9445-a097-4e1c-93f9-af44174178d3/03a7e420-db51-4919-858a-abcdd23797b8.jpg/:/cr=t:0%25,l:0%25,w:100%25,h:100%25/rs=h:1000,cg:true)

## CORAL GABLES OKTOBERFEST

>**Date:** \
October 3 to 13 \
**location:** \
 Fritz and Franz Bierhaus, \
 60 Merrick Way
 Coral Gables, FL 33134 \
 [MAP](https://goo.gl/maps/MQjFSMcTZsedbYSZ8)

Oktoberfest in Coral Gables presents the 25th anniversary of their 10-day beer, food, and music festival with plaza entertainment.

[Read More](https://myoktoberfestmiami.com)

-----------------

![Books&Books](https://i0.wp.com/i.imgur.com/xDTREMH.jpg?zoom=1.8700001239776611&ssl=1)

## Coral Gables Gallery Night
Lauren Faith Dawson

>**Date:** \
Friday 10-4, 7:00 pm - 10:00 pm \
>**Location:** \
Books & Books in Coral Gables \
265 Aragon Ave \
Coral Gables, FL 33134 United States \
Phone:  3054424408 \
[MAP](https://goo.gl/maps/HUJnszBkKZy)

Lauren Faith Dawson is a fine artist based in Miami, FL. Her work explores themes of femininity and motherhood. Lauren’s degree in sculpture helps her approach her paintings as objects instead of surfaces. She incorporates materials such as wax, latex, fabric and thread into her canvases. Her work is reflective, playful and meditative.


[READ MORE](https://www.facebook.com/events/2110603972578604/)

-----------------

![Imgur](https://i.imgur.com/EEe4CKN.png?2)

## GALLERY NIGHT LIVE

>**Date:** \
Friday 10-4 @ 7:00 pm - 11:00 pm \
>**Location:** \
Coral Gables Museum, \
285 Aragon Avenue \
Coral Gables, FL 33134 \
[MAP](https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=285+Aragon+Avenue+Coral+Gables+FL+33134+United+States)

Join us for the Grand Opening of The ​Caribbee Club: ​Body Landscapes; Designs by Andrea Spiridonakos at the Frank Lynn Gallery. Also on view are ​America Weaves, curated by Adriana Herrera, at the Fewell Gallery; ​The Art of Compassion, curated by Starr Sariego, in Gallery 109 and Abraham Gallery; ​Doble filo: geografías y palabras, curated by Felix Suazo, in the Community Meeting Room; and our permanent exhibition, ​Creating the Dream.

[READ MORE](https://coralgablesmuseum.org/event/mercedes-benz-of-coral-gables-gallery-night-live-9-2018-11-02/2019-10-04/)


---------------------------------------

![National Theater Live](http://www.gablescinema.com/media/filmassets/NTL_2019_Im_Not_Running_-_NEW_Website_Listings_Images_-_Portrait.jpg.500x715_q85_crop-smart.jpg)

## I'M NOT RUNNING
National Theater Live

>**Date:** \
 Friday 10-4, 12 p.m. \
>**Location:**
Gables Cinema \
260 Aragon Ave. \
Coral Gables, FL 33134 \
[MAP](https://goo.gl/maps/A4s1cECHe5H2), \
**Cost:** $20

Pauline Gibson (Siân Brooke) has spent her life as a doctor, the inspiring leader of a local health campaign. When she crosses paths with her old boyfriend, a stalwart loyalist in Labour Party politics, she’s faced with an agonizing decision.

[READ MORE](http://www.gablescinema.com/events/im-not-running/)

------------------------

![ORCHID SHOW](https://www.eventcrazy.com/event/photos/775959_1_61_081119_060618.jpg)

## The 73rd Miami International Orchid Show

>**Date:**
Saturday Oct 5th & 6th, 10 a.m. - 5 p.m. \
**location:**
Wasco Center \
1245 Dauer Dr, Coral Gables, FL 33146 \
[MAP](https://goo.gl/maps/YeFEQ3pUMZuWmwLh6)

Orchid show sponsored by south florida Orchid society featuring Internationally Acclaimed Orchid Growers From around the World and the U.S. Thousands of orchids for sale. Beautiful tabletop orchid exhibits. AOS Judged Show. How to grow Lectures will be given on Saturday Bring your orchid plants in for judging on Friday registration only.

[Read More](http://www.sforchid.com/)

----------------------

![MIAMI ENTERTAINMENT MONTH](https://www.arshtcenter.org/Global/Subscriptions/Broadway%20in%20Miami%202019-20/BIM-1920-Subs-690x310-v3.jpg)

## MIAMI ENTERTAINMENT MONTHS

>**Date:** \
October 1, 2019 - November 30, 2019 \

October kicks of the two-month long Miami Entertainment Months. You’ll enjoy promotions on everything from craft brewery tastings to movie screenings at state-of-the-art cinemas. Miami’s entertainment scene runs the gamut from concerts to the theater, dance, nightlife and shopping.

[Read More](https://www.miamiandbeaches.com/offers/temptations/miami-entertainment-months)

-------------------------------------

![BURGER KING BEACH RUN](http://teamfootworks.org/wp-content/uploads/2016/05/BKBeachRunLogo-800x300_c.png)

## BURGER KING BEACH RUN

>**Date:** \
Saturday, 10-5 7:30 AM \
**location:** \
lummus Park
1130 Ocean Dr, Miami Beach, FL 33139 \
[MAP](https://goo.gl/maps/Urr1JWj6LUV1gLUn8)

Take part in a heart-pumping 5K or 10K race starting at Lummus Park in South Beach. The Burger King Beach Run is happening in the morning on October 6 along with a Kiddie Dash. The post-race beach party is free for everyone.

[Read More](http://teamfootworks.org/burger-king-beach-run/)

--------------------------------

![Farmers Market](https://d36ki7ut62e74z.cloudfront.net/prod/optimized/b170_farmers_market_sign.jpg)

## THE MARKET AT MERRICK

Farmer's Market

>**Date:** \
 Sundays 11 a.m. to 6 p.m. \
>**Location:** \
358 San Lorenzo Ave, Coral Gables, FL 33146 \
[MAP](https://goo.gl/maps/zRXV4KAWXJBouUu68)

Visit our fabulous Farmers.Stop by and pick up some beautiful and tasty produce, healthy homemade products, dessert and more! The Farmers' Market is located between Tourneau and Nordstrom

[READ MORE](https://www.shopsatmerrickpark.com/en/events/the-farmers-market-at-merrick-park-18504.html)

-----------------------

![Imgur](https://i.imgur.com/8emAAaf.png)

There’s going to be less and less land to build, and that’s going to mean a higher premium. Northerners and foreigners coming down here, that’s going to drive prices,” said Kevin Morris, a South Florida Senior Director for Colliers International Affordable Housing division. JOE RAEDLE GETTY IMAGES

## Miami’s population is changing — and that’s making real estate change, too.

[Read More](https://www.miamiherald.com/news/business/real-estate-news/article235149742.html)
