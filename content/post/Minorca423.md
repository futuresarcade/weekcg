---
title: "2030 S. Douglas Rd. #423 Coral Gables"
date: 2018-08-26T11:18:12-04:00
draft: true
tags: ["Coral-Gables","Rent"]
categories: ["Rent", RealEstate", "CoralGables"]
author: "Futuresarcade"
autoCollapseToc: true
contentCopyright: ''
---

[![Banner](https://www.weekincoralgables.com/DianaBanner2.png)](http://yourhomeincoralgables.com/)

![Imgur](https://i.imgur.com/p1Ph0rj.jpg?1)

## FEATURED LISTING FOR RENT $2700

### - Just Reduced -
### Furnished or Unfurnished

### 2030 S. Douglas Rd. #423 Coral Gables

#### Live in Down Town Coral Gables

Two bedrooms, two bathrooms, den and big terrace.  Its central location & close proximity to Miracle Mile allows you to walk or take the trolley to a great selection of the best restaurants, café’s, shops, fine art galleries & more, close to public transportation.


[READ MORE](http://yourhomeincoralgables.com/listings/)


## EWM #1 BROKERAGE IN CORAL GABLES

![Imgur](https://i.imgur.com/KMHHgA0.jpg?1)

## Call Us For your Real Estate needs
### Diana Ordonez Tel: 305-510-4698

--------------

<!--more-->r
