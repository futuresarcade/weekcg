---
title: "20190607_wcg"
date: 2019-06-05T16:43:18-04:00
draft: false
tags: ["Coral-Gables","Events"]
categories: ["Events"]
author: "Futuresarcade"
autoCollapseToc: false
contentCopyright: ''
---

[![Banner](https://www.weekincoralgables.com/DianaBanner2.png)](http://yourhomeincoralgables.com/)

-------------------


![Restaurant Week](http://restaurantweek.shopcoralgables.com/wp-content/uploads/2019/05/BID_CulinaryMonth_WebsiteBanner_1920x500_05.07.19_Draft1-1920x500.jpg =300x)


## CORAL GABLES RESTAURANT WEEK

>**Date:**  \
JUNE 4 –24 \
>**Location:**  \
[Restaurants](http://restaurantweek.shopcoralgables.com/restaurants/)

The rest of the month of June is filled with colorful cocktails and delicious food specials at restaurants on Miracle Mile and throughout Downtown Coral Gables. Coral Gables Restaurant Week features more than 40 eateries offering special three-course, prix-fixe lunch and dinner menus at discounted prices.

[READ MORE](http://restaurantweek.shopcoralgables.com/)

----------------------

![Imgur](https://i.imgur.com/cjRbQBB.jpg?2)

## CORAL GABLES GALLERY NIGHT

> **Date:** \
Friday 6-7 6:30 pm – 10:00 pm \
**Location:** \
Various galleries \
Coral Gables, Florida \
**Cost:** Free

[Gallery Night Trolley](http://www.citybeautiful.net/Modules/ShowDocument.aspx?documentID=23109)

Held the first Friday of the month, Gallery Nights showcase the various works from European and Latin American art masters to outstanding contemporary artists. Be on the lookout for the orange paintbrush on the side of the Trolley and on special Trolley stop signs to find a dedicated Gallery Night Trolley that will loop between Coral Gables galleries and art venues participating on Gallery Night.

----------------


![Imgur](https://i.imgur.com/EEe4CKN.png?2 =300x)

## GALLERY NIGHT LIVE

>**Date:** \
Friday June 7 @ 7:00 pm - 11:00 pm \
>**Location:** \
Coral Gables Museum, \
285 Aragon Avenue \
Coral Gables, FL 33134 \
[MAP](https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=285+Aragon+Avenue+Coral+Gables+FL+33134+United+States)

Join us on Gallery Night and attend the Grand Opening of the shows The Caribbee Club: Krome Avenue; Paintings by Andres Cabrera at the Frank Lynn Gallery; America Weaves, at the Fewell Gallery; and The Art of Compassion in Gallery 109 and Abraham Gallery. Also on view that night is our permanent exhibition: Creating the Dream; George Merrick and his Vision for Coral Gables. Among other amenities in the courtyard, there will be live music in collaboration with the University…

[READ MORE](http://coralgablesmuseum.org/event/mercedes-benz-of-coral-gables-gallery-night-live-9-2018-11-02/2019-06-07/)

-----------------

![Books&Books](https://i0.wp.com/i.imgur.com/xDTREMH.jpg?zoom=1.8700001239776611&ssl=1 =300x)

## Coral Gables Gallery Night
Ana Trelles Portuondo

>**Date:** \
Friday June 7 @ 7:00 pm - 10:00 pm \
>**Location:** \
Books & Books in Coral Gables \
265 Aragon Ave \
Coral Gables, FL 33134 United States \
Phone:  3054424408 \
[MAP](https://goo.gl/maps/HUJnszBkKZy)

“Capturing the expression is everything…….It’s the gateway to the soul”

The ARCHITECT SERIES is a monochromatic representation of the classical figures that have influenced Architecture and their enigmatic genius. She captures their essence and their personalities in her use of graphite and charcoal on paper.


[READ MORE](https://booksandbooks.com/event/coral-gables-gallery-night-featuring-ana-trelles-portuondo/)

-----------------

![Firt Friday Series](https://www.coralgables.com/media/Community%20Recreation/AAC/PHOTOS/First%20Fridays%20Series%20Presents%20James%20Woolwine.jpg?width=300)

## First Friday Series
James Woolwine

>**Date:** \
Friday June 7, 6:00 - 8:00 pm \
>**Location:**  \
Adult Activity Center \
2 Andalusia Ave, \
Coral Gables, FL 33134 \
[MAP](https://goo.gl/maps/xqPitYqF8vguq7mKA)

James Woolwine is an accomplised musician with several singles and two albums, solo composer/song writer on guitar and piano.

[READ MORE](https://www.coralgables.com/calendar)

-------------------


![Imgur](https://i.imgur.com/bBAF6t7.jpg?3 =300x)


## FIRST FRIDAY ICA (Institute of Contemporary Art)

>**Date:**
Friday 6-7, 6:00 pm-10 pm \
>**Location:** \
ICA MIAMI \
61 NE 41st Street \
Miami, FL 33137 \
Cost: Free \
[MAP](https://goo.gl/maps/JjDN2U1GH7N2)

Celebrate ICA Miami’s first season of First Fridays programming with a sample of Miami’s most exciting performing artists.

[READ MORE](https://www.miamidesigndistrict.net/event/1381/first-fridays-at-ica-miami/)

------------------

![Starseed](https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F62744525%2F292400258795%2F1%2Foriginal.20190522-230801?w=800&auto=compress&rect=0%2C0%2C1216%2C608&s=b71f8ce3a7548ec6c4e688d0c7134e56 =300x)

## Starseed Music and Arts Fest

>**Date:**  \
June 8, 2019, 2:00 - 10 PM \
>**Location:** \
The Barnacle Historic State Park \
3485 Main Highway \
Coconut Grove, FL 33133 \
[MAP](https://goo.gl/maps/bh3AghS7eKN2)

A unique fusion of Artists across multiple platforms, Starseed combines a fully curated art exhibit and a talent-packed live music line-up alongside a vendor market, local food, installations, and digital creations.

[READ MORE](https://www.eventbrite.com/e/starseed-music-and-arts-fest-tickets-62026100737?aff=ebdssbdestsearch)

--------------------

![OUT IN THE TROPIC](http://fundarte.us/wp-content/uploads/2018/05/Ecard-Image-OIT18.jpg =300x)

## Out In The Tropics

>**Date:** \
May 24 – June 24 \
>**Location:** \
Miami Beach

A month of presentations by artists addressing issues ranging from politics to sexuality. The 10th season of Out in the Tropics presented by FUNDarte will feature a variety of performances at venues such as Centro Cultural Español, The Gleason Room at The Fillmore, The Miami Beach Botanical Garden and more. Tickets start at $25.

[READ MORE](http://fundarte.us/fundarte-event/out-in-the-tropics/)

-----------------------

![Litte Flower Food & Wine](https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F58768262%2F229443403248%2F1%2Foriginal.20190319-201613?w=800&auto=compress&rect=0%2C0%2C2160%2C1080&s=8cc241864846ac5d54367ffb7e603589 =300x)

## Little Flower Food and Wine Festival

>**Date:**  \
Sat, June 8, 2019
7:30 PM – 11:30 PM \
>**Location:**  \
Comber Hall \
1251 Palermo Avenue \
Coral Gables, FL 33134 \
[MAP](https://goo.gl/maps/PwfqW1L9a542yejR6) \
>**Cost:**$50

 A night of food, wine, entertainment, family & friends. Kick off the summer with the 2nd Annual Little Flower Food & Wine Festival a truly Culinary experience for the senses.

 [READ MORE](https://www.eventbrite.com/e/little-flower-food-wine-festival-tickets-59077243626)


---------------------

![Royal Ponciana Fiesta](https://communitynewspapers.com/wp-content/uploads/2019/05/poster-RPF_page-0001-1-min.jpg =300x)

## Royal Ponciana Fiesta

>**Date:**  \
Saturday June  8 to Monday June 10 \
>**Location:**
[Events](http://www.tfts.org/rpf-2/)

Each year like clockwork, the June skies light up South Miami, Coconut Grove, and Coral Gables with the rich colors of deep scarlet, fire engine red, red, pumpkin, orange, golden yellow, canary yellow and gradations in between.  (The different colors represent the tropical lands where our Royal Poincianas were originally sourced including Madagascar, India, Ethiopia, and St. Maarten.)  And, like clockwork, the Royal Poinciana Fiesta, Miami-Dade’s oldest festival, comes alive when the colorful blooms are at their peak.  Now in its 82nd year, the 2019 Royal Poinciana Fiesta will be from Saturday, June 8 through Monday June 10, 2019.

[READ MORE](https://communitynewspapers.com/south-miami/royal-poinciana-festival-set-for-the-weekend-of-june-8-10/)

------------------------

![Imgur](https://i.imgur.com/BAB2LMX.jpg?1)

## CITY THEATRE’S SUMMER SHORTS
>**Date:**
MAY 30 – JUNE 30 \
>**Location:**
Carnival Studio Theater \
1300 Biscayne Blvd, \
Miami, FL 33132 \
[MAP](https://goo.gl/maps/hUtk97hPc1CCH7Ub9)

Celebrating its 23rd season of ten-minute entertaining plays, City Theatre’s Summer Shorts, America’s Short Play Festival, is happening throughout the month of June. Tickets start at $39 per person and are available through the Adrienne Arsht Center for the Performing Arts.

[READ MORE](https://www.citytheatre.com/summer-shorts)

------------------------


![Imgur](https://i.imgur.com/4P0KB6I.jpg)

## METROPOLIS APARTMENT - FOR SALE
#### 9066 SW 73 Ct # 605 Miami, Florida 33156

Loft style apartment at Dadeland. 13 FT ceilings add to the spacious feeling of this open plan 1 bedroom, 1 bath + den. Metropolis Dadeland, walking distance to Restaurants and shopping. 2 swimming pools, Jacuzzi, fitness center, recreational room 24 hr concierge services and valet parking.

[READ MORE](http://yourhomeincoralgables.com/listings/9066-sw-73-ct-605-miami-florida-33156/)

----------------------
