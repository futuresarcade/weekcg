---
title: "20190526_wcg"
date: 2019-05-23T09:05:22-04:00
draft: false
tags: ["Coral-Gables","Events"]
categories: ["Events"]
author: "Futuresarcade"
autoCollapseToc: false
contentCopyright: ''
---

[![Banner](https://www.weekincoralgables.com/DianaBanner2.png)](http://yourhomeincoralgables.com/)

-------------------

![OUT IN THE TROPIC](http://fundarte.us/wp-content/uploads/2018/05/Ecard-Image-OIT18.jpg =300x)

## Out In The Tropics

>**Date:** \
May 24 – June 24 \
>**Location:** \
Miami Beach

A month of presentations by artists addressing issues ranging from politics to sexuality. The 10th season of Out in the Tropics presented by FUNDarte will feature a variety of performances at venues such as Centro Cultural Español, The Gleason Room at The Fillmore, The Miami Beach Botanical Garden and more. Tickets start at $25.

[READ MORE](http://fundarte.us/fundarte-event/out-in-the-tropics/)

-----------------------

![AIR & SEA](https://usasalute.com/wp-content/uploads/2019/04/air-show-music-explosion.jpg =300x)

## Air & Sea Show And The Musical Explosion

>**Date:** \
May 25-26 \
>**Location:** \
Miami Beach

Happening on Memorial Day Weekend, the Air & Sea Show is a two-day event presented by Hyundai on Miami Beach that will showcase and salute all five branches of the United States military with jet flyovers and stunts, extreme water sports and more. Dubbed “The Musical Explosion,” there will also be a concert as well-known artists perform on stage while jets fly overhead. Expect fireworks, sand sculpting, lumberjack shows, motocross exhibition and a kids fun zone. The event is free and open to the public.

[READ MORE](https://usasalute.com/wp-content/cache/all/event/index.html)

----------------


![Food Truck Invasion](http://www.foodtruckinvasion.com/wp-content/uploads/2016/04/miaSTfoodCT_FB.jpg =300x)

## FOOD TRUCK INVASION

>**Date:** \
Every fourth Friday of the month: 5:30-10:30 p.m. \
>**Location:**  \
Tropical Park \
7900 Bird Rd.\
Miami, FL 33155.
[MAP](https://goo.gl/maps/2JbGZpaLNDtmkMGm9)

Examples of food trucks that may attend some of the events above include:El Orgullo Latino Kitchen-Top Fries-Cold Stone Creamery-Don Mofongo-a Burger Shack------

[READ MORE](http://www.foodtruckinvasion.com/)

-----------------

![REFRAME](https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F62374262%2F46092878297%2F1%2Foriginal.20190516-035150?w=800&auto=compress&rect=0%2C60%2C1920%2C960&s=fc2194f2476d9897c5d2eeae1bb3c794 =300x)


## ReFrame: A Memorial Day Filled With Arts & Culture on South Beach

>**Date:** \
May 25-26 \
>**Location:** \
Miami Beach

 A series of art and cultural happenings that reimagine Memorial Day Weekend on Miami Beach. New this year, ReFrame sparks crucial conversations about inclusion, surveillance, and propaganda using the works of local artists, curators, and organizers. Events start on Thursday, May 23 to Monday, May 27, with the creative minds from Miami New Drama and BASS Museum, and FIU Miami Beach Urban Studios among others.

[READ MORE](https://www.eventbrite.com/e/reframe-a-memorial-day-filled-with-arts-culture-on-south-beach-tickets-61719682231)

------------------------

![BEST OF THE MUSIC](https://bestofthebestconcert.com/wp-content/uploads/2017/11/BOTB-2019-LOGO-1-e1545235146855.png =300x)

## Best Of The Best Music Fest

>**Date:** \
May 26 \
>**Location:** \
Bayfront Park \
301 Biscayne Blvd, \
Miami, FL 33132 \
[MAP](https://goo.gl/maps/ajn94Jms24JWmbLu9)

Featuring local and national acts, tREhe annual Best of the Best Music Fest showcases bands across a range of genres including hip-hop, reggae, pop and more. The concert takes place on the waterfront at Miami’s Bayfront Park. General admission tickets are available for $30 and VIP tickets start at $120.

[READ MORE](http://bestofthebestconcert.com/)

-------------------------

![MEMORIAL DAY](http://www.destination360.com/travel/events/memorial-day/images/s/miami.jpg =300x)

## Memorial Day

>**Date:** \
May 28 \
>**Location:** \
Differet Venues

Not only is Memorial Day a federal holiday dedicated to remembering those who gave their life while serving in the armed forces, it’s also the unofficial kick-off for summer vacation season. In Miami, there are a variety of ways to commemorate the three-day weekend. Check out some of our recommendations on how to spend Memorial Day Weekend in Miami.

[READ MORE](https://www.miamiandbeaches.com/events/fun-things-to-do-for-memorial-day)

----------------

![FAMILY FEST STEP AFRIKA](https://www.arshtcenter.org/Global/Plays/2019/Family%20Fest/Step%20Afrika/Step-Afrika-690x310.jpg =300x)


## Family Fest: Step Afrika

>**Date:** \
May 25, 2019, Starting: 01:30 PM \
>**Location:** \
ADRIENNE ARSHT CENTER FOR THE PERFORMING ARTS OF MIAMI-DADE COUNTY \
KNIGHT CONCERT HALL \
1300 Biscayne Blvd.\
Miami, FL 33132 \
>**Cost:** FREE

A homage to the African American step show while introducing the audience to Zulu and South African Gumboot Dance, Step Afrika shares step tradition of audience participation with complex polyrhythmic symphony that brings audiences to their feet. Step Afrika is a professional dance company dedicated to the tradition of stepping. Step Afrika! accomplishes this mission through arts education activities, international cultural exchange programs and performances. Over the past 19 years Step Afrika! has grown to become one of the top 10 African American Dance Companies in the US and the largest African American led arts organization in Washington, DC.

[READ MORE](https://www.arshtcenter.org/Tickets/Calendar/2018-2019-Season/Family-Fest/Step-Afrika/?performanceNumber=27838)

--------------------

![MIAMI MUSIC PROJECT](https://miamimusicproject.org/wp-content/uploads/2018/09/mmp-web-logo.png =300x)

## Miami Music Project: Fantastic Season Finale

>**Date:** \
May 25, 2019 , From 12:00 PM  \
>**Location:** \
FIU WERTHEIM AUDITORIUM \
10910 SW 17th St. \
Miami , FL 33199 \
>**Cost:**$5 suggested donation

Come celebrate the Miami Music Project's season finale' of students at all levels.

12:00 PM
Intermediate and Advanced Level Ensembles
Including the Miami Music Project Leaders Orchestra
6:00 PM
Beginner Level Ensembles

[READ MORE](http://www.miamimusicproject.org/)

----------------------

![MEMORIAL DAY](http://www.destination360.com/travel/events/memorial-day/images/s/miami.jpg =300x)

## MEMORIAL DAY CORAL GABLES

>**Date:** \
Monday May 27, 09:00 AM - 10:00 AM \
>**Location:** \
Coral Gables War Memorial Youth Center \
Address405 University Drive \
Coral Gables, Florida 33134 \

Please join the City of Coral Gables in honoring the men and women who have their lives in service protecting our nation's freedom.


[READ MORE](https://www.coralgables.com/calendar)

---------------------

![Imgur](https://i.imgur.com/4P0KB6I.jpg)

## METROPOLIS APARTMENT - FOR SALE
#### 9066 SW 73 Ct # 605 Miami, Florida 33156

Loft style apartment at Dadeland. 13 FT ceilings add to the spacious feeling of this open plan 1 bedroom, 1 bath + den. Metropolis Dadeland, walking distance to Restaurants and shopping. 2 swimming pools, Jacuzzi, fitness center, recreational room 24 hr concierge services and valet parking.

[READ MORE](http://yourhomeincoralgables.com/listings/9066-sw-73-ct-605-miami-florida-33156/)

----------------------
