---
title: "20190816_wcg"
date: 2019-08-15T10:12:38-04:00
draft: false
tags: ["Coral-Gables","Events"]
categories: ["Events"]
author: "Futuresarcade"
autoCollapseToc: false
contentCopyright: ''
---

<head>
    <style type="text/css">
      table {
        border:2px solid #552448;
        margin-top:10px;
        margin-bottom:50px;
      }
      article {
        clear: left;
      }
      img {
        width: 300px;
      }
    </style>
  </head>
<body>
<table width="700">
<tr valign="middle">
<td rowspan="2" width="220"  bgcolor= "#652f56" style="border-right: #552448 1px solid;">
<img src="https://i.imgur.com/r3dTAc6.png" title="source: imgur.com" width="250" alt="r3dTAc6.png">
</td>
<td rowspan="2" width="10">
</td>
<td>
<font style="color: #552448; font-family: Georgia; font-size: .95em"><strong>Diana Ordonez-Sanchez</strong></font><br />
<font style="color: #939598; font-family: Arial; font-size: .85em">Realtor Associate</font><br />
<font style="color: #939598; font-family: Arial; font-size: .85em">Chairman's Club</font><br />
<font style="color: #939598; font-family: Arial; font-size: .85em"><a href="tel:1-305-510-4698">1-305-510-4698</a> </font><br />
<font style="color: #939598; font-family: Arial; font-size: .85em">Berkshire Hathaway HomeServices EWM Realty | Realty Division</font><br />
</td>
</tr>
<tr>
<td height="40" valign="bottom">
<font style="color: #939598; font-family: Arial; font-size: .85em"><span style="color: #552448;">O:</span> 305-960-2400 |<span style="color: #552448;">E:</span> dianaiordonez@gmail.com | <a href="http://www.dianaordonez.com" style="text-decoration: none; color: #552448;">www.dianaordonez.com</a></font><br />
<font style="color: #939598; font-family: Arial; font-size: .85em">550 S. Dixie Hwy, Coral Gables, Florida 33146</font><br />
</td>
</tr>
</table>

</body>

----------------------------

![Imgur](https://i.imgur.com/sBfDw0V.jpg?1)

## MIAMI SPICE
>**Date:**  \
AUG. 1 - SEPT. 30 \
>**Location:**  \
[Participating Restaurants](https://www.miamiandbeaches.com/offers/temptations/miami-spice-months/list-at-a-glance)

August kicks off two months of Miami Spice, when restaurants in all corners of the destination offer special three-course, prix-fixe menus featuring their tastiest dishes at marked down prices (lunch/brunch $23, dinner $39). Make a reservation, get out there, and taste, taste, taste!

[READ MORE](https://www.miamiandbeaches.com/offers/temptations/miami-spice-months)

-----------------------------------------

![National Rum Day](https://user-images.strikinglycdn.com/res/hrscywv4p/image/upload/c_limit,fl_lossy,h_9000,w_1920,f_auto,q_auto/774070/NationalRumDayFest20_dnsebw.jpg)

## NATIONAL RUM DAY FESTIVAL
>**Date:** \
AUGUST 16 \
>**Location:**  \
The Confidante - Hyatt \
4041 Collins Ave, \
Miami Beach, FL 33140 \
[MAP](https://goo.gl/maps/VF5vv5d1tZmXu1cU6) \
>**Cost:** $35 - $55

Cool down on a hot August day with the 3rd Annual National Rum Day Festival. Experiment with new rum brands and flavors poolside at The Confidante Miami Beach hotel. Sip on sweet cocktails from the best rum brands on the market and celebrate Miami’s favorite spirit.

[READ MORE](https://www.nationalrumdayfest.com/)

---------------------


![National Theater Live](http://www.gablescinema.com/media/filmassets/NTL_2019_The_Lehman_Trilogy_Website_Listing_Image_-_1240x874px.jpg.460x287_q85_crop-smart.jpg)

## THE LEHMAN TRILOGY
National Theater Live

>**Date:** \
Friday 8-16 - Sunday 8-18, 12 p.m. \
>**Location:**
Gables Cinema \
260 Aragon Ave. \
Coral Gables, FL 33134 \
[MAP](https://goo.gl/maps/A4s1cECHe5H2), \
**Cost:** $20

Academy Award-winner Sam Mendes (Skyfall, The Ferryman) directs Simon Russell Beale, Adam Godley and Ben Miles who play the Lehman Brothers, their sons and grandsons. On a cold September morning in 1844, a young man from Bavaria stands on a New York dockside.

[READ MORE](http://www.gablescinema.com/events/the-lehman-trilogy/)

---------------

![International Ballet Festival](https://static.wixstatic.com/media/0166bf_ddb852aafdf2433e9da2af333285b119~mv2_d_2835_2835_s_4_2.jpg/v1/crop/x_0,y_91,w_2835,h_2652/fill/w_721,h_672,al_c,q_85,usm_0.66_1.00_0.01/FESTIVAL-GENERICO-NUEVO-AMARILLO.webp)

## International Ballet Festival:

>**Date:** \
July 27 – August 18 \
>**Location:**  \
Different venues

Showcasing the works of more than 200 ballet artists from 20 companies from around the world, the International Ballet Festival returns to Miami in 2019. Performances will take place at venues like Fillmore Miami Beach at Jackie Gleason Theater, Lehman Theater at Miami Dade College North Campus and Colony Theater. Ticket prices vary.

[READ MORE](https://www.internationalballetfestival.org/)

--------------------

![Imgur](https://i.imgur.com/ADp0RWq.png?1)

## BOOKS AND BOOKS

>**Date:** \
>Friday and Saturday 7:00 p.m. \
**Location:** \
Books & Books in Coral Gables \
265 Aragon Ave \
Coral Gables, FL 33134 \
[Map](https://goo.gl/maps/2XgRch3XXYS2)

The Café’s Wine Bar, located at the entrance to the courtyard, features craft beer and wines from around the world offered by the glass or by the bottle. A free, live music series showcases musicians of all types and genres, including Jazz, African, Latin, Caribbean and World Beat. Every Friday and Saturday beginning at 7:00pm.

[READ MORE](http://www.thecafeatbooksandbooks.com/events.html)

------------------

![CoconutGrove Food & Wine Festival](https://img1.wsimg.com/isteam/ip/0c1a1af4-88b6-40bb-aae1-71e8214e4e09/8dc5734d-7c86-471a-8173-de3c32cc405c.jpg/:/rs=w:1050,cg:true,m)

## COCONUT GROVE FOOD & WINE FESTIVAL

>**Date:**  \
 AUGUST 17 \
>**Location:** \
2985 S. BAYSHORE DR. \
COCONUT GROVE, FL 33133 \
[MAP](https://goo.gl/maps/3ignB5WNQ2U3b5JL8) \
>**Cost:**$25 - $75

Come hungry (and thirsty) to the Coconut Grove Food & Wine Festival, where you’ll be eating and drinking as you move through Coconut Grove at this progressive food festival. Start at the Coconut Grove Woman’s Club with tastings and demonstrations and then make your way to the Mutiny Hotel’s pool area for a tropical taste from the rum bar, craft beer stations, restaurant samplings and live music, with the sounds of the pool’s waterfall in the background. It’s a perfect way to spend a day in one of Miami’s waterfront neighborhoods.

[READ MORE](https://coconutgrovefoodandwine.com/)

---------------------

![Imgur](https://i.imgur.com/5Zc2t4c.jpg)

## ALMA DE TANGO - THE WHITE PARTY

>**Date:**  \
Sunday August 18th, 8:00 p.m - 1:00 a.m \
>**Location:** \
Biltmore hotel \
1200 Anastasia Ave, \
 Coral Gables, FL 33134 \
 [MAP](https://goo.gl/maps/htKLQFv9dAATq2Jx9) \
 >**Cost:**$25, Cash Bar

Our Signature WHITE PARTY is an enchanted evening of pure white elegance. This year's event will be held in no other than the historic Biltmore Hotel's breathtaking Grand Country Club Ballroom. In honor of the special evening, the grand ballroom will be dressed in elegant white linens, flooded with delicate white flowers and lit with romantic white candles. Simply dreamlike.

Continuing with the theme, everyone must come dressed in their utmost finest white attire.

[READ MORE](https://www.facebook.com/events/383686205579637/)

-------------------


![Farmers Market](https://d36ki7ut62e74z.cloudfront.net/prod/optimized/b170_farmers_market_sign.jpg)

## THE MARKET AT MERRICK

Farmer's Market

>**Date:** \
 Sundays 11 a.m. to 6 p.m. \
>**Location:** \
358 San Lorenzo Ave, Coral Gables, FL 33146 \
[MAP](https://goo.gl/maps/zRXV4KAWXJBouUu68)

Visit our fabulous Farmers.Stop by and pick up some beautiful and tasty produce, healthy homemade products, dessert and more! The Farmers' Market is located between Tourneau and Nordstrom

[READ MORE](https://www.shopsatmerrickpark.com/en/events/the-farmers-market-at-merrick-park-18504.html)

----------------------------

![Metropolis](https://i.imgur.com/bKMfKUX.gif =400x)

## METROPOLIS APARTMENT - FOR SALE

>**Location:** \
9066 SW 73 Ct # 605 Miami, Florida 33156

Loft style apartment at Dadeland. 13 FT ceilings add to the spacious feeling of this open plan 1 bedroom, 1 bath + den. Metropolis Dadeland, walking distance to Restaurants and shopping. 2 swimming pools, Jacuzzi, fitness center, recreational room 24 hr concierge services and valet parking.

[READ MORE](http://yourhomeincoralgables.com/listings/9066-sw-73-ct-605-miami-florida-33156/)

----------------------------

![Marriott Markis](http://i.imgur.com/OmPpVKQ.jpg)

### HERE’S A LIST OF EVERY TOWER ANNOUNCED AT MIAMI WORLDCENTER TO DATE, NOW NEARING A DOZEN

Miami Worldcenter is in the process of transforming downtown Miami, with block after block of massive towers already underway or in planning. It is either the biggest or second biggest urban development project in the United States (some say that New York’s Hudson Yards is bigger).

[READ MORE](https://www.thenextmiami.com/heres-a-list-of-every-tower-announced-so-far-at-miami-worldcenter-now-nearing-a-dozen/?fbclid=IwAR3oaGWFq4qTCMT4NvIAO-hN9HbYdECcUnZVTwrrHqVAlVavCITrU7n5kUw)
