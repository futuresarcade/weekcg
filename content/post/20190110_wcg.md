---
title: "20190110_wcg"
date: 2019-01-09T14:49:44-05:00
draft: false
tags: ["Coral-Gables","Events"]
categories: ["Events"]
author: "Futuresarcade"
autoCollapseToc: true
contentCopyright: ''
---


[![Banner](https://www.weekincoralgables.com/DianaBanner2.png)](http://yourhomeincoralgables.com/)

-----------------

![Imgur](https://i.imgur.com/nmbcCJN.png?1)

## The 68th annual Beaux Arts Festival

>**Date:**
January 12 and 13, 2019 10:00 AM - 5:00 PM

>**Location:**
Grounds of University of Miami
1301 Stanford Dr
Coral Gables, Florida
[MAP](https://goo.gl/maps/KBu1ACZSiHw)

Juried fine arts show that features art, crafts and a variety of media from more than 300 exhibitors on the University of Miami campus. Live music and other free entertainment, children's art experiences, fabulous dining options, and free admission all weekend to the Lowe Art Museum.


------------------

![Imgur](https://i.imgur.com/NlqeQBj.png?1)

## Uncorked Wine Festivals

>**Date:**
January 12th 2019, 1-5pm
**Location**
Regatta Park
2700 South Bayshore Drive
Miami, FL 33133
[MAP](https://goo.gl/maps/LkhL6mRdWrK2)
**Cost:** $70 VIP General Admission $60
**Restrictions** 21 and over only, No pets allowed

Enjoy over 100 wines from across the globe, bubbly bar, gourmet food trucks, live DJ, lawn games and more. unlimited wine tasting with food sold separately

VIP admission ($70) is at 1pm and includes an extra hour of tasting plus some special wines during that first hour from select wineries. General admission ($60) is at 2pm and the event ends at 5pm.

[READ MORE](https://uncorkedwinefestivals.com/miami-wine-festival/)

--------------

![Imgur](https://i.imgur.com/fSRYChI.jpg?1)

# International Orchid Festival

>**Date:**
January 11-13, 2019 10am to 6pm
**Location**
Miami-Dade County Fair Expo Center
10901 Coral Way (S.W. 24th St) Miami, FL 33165
[MAP](https://goo.gl/maps/7tL8zv4uipr)
**Cost:** $12

>"The largest winter orchid event in the United States."-  The Boston Globe

[READ MORE](http://tamiamiorchidfestival.com/)

-------------------
![Imgur](https://i.imgur.com/MQFpLtp.jpg?1)

## FAMILY DAY ARAGON

>**Date:**
Saturday 1-12, 2:00 pm-5:00 pm
**Location:**
Coral Gables Museum
285 Aragon Avenue
Coral Gables, FL 33134
**Cost:** Free
[MAP](https://goo.gl/maps/jZyDgXWtZeE2)

The Coral Gables Art Cinema takes place this weekend "Family Day on Aragon", event co-organized with Books & Books and Coral Gables Museum and dedicated to children.

[READ MORE](http://coralgablesmuseum.org/event/family-day-free-3/)

-----------------
![Imgur](https://i.imgur.com/uSXzzh3.jpg?1)

 ## MATILDA

 >**Date:**
 Saturday 1-12 & Sunday 1-13 @ 11:00 AM
 >Location:
 Gables Cinema
 260 Aragon Ave
 Coral Gables, FL 33134-5008
 Cost: $5
 >[Map](https://goo.gl/maps/9rpMGMGn4RU2)

Based on the story of the writer Roald Dahl, the girl Matilda Wormwood (Mara Wilson) has a sense of humor and supernatural powers enough rogues that tyrannize her dad (Danny DeVito) and mom (Rhea Perlman).

[READ MORE](http://www.gablescinema.com/events/matilda/)

--------------
![Imgur](https://i.imgur.com/ADp0RWq.png?1)

## LIVE MUSIC IN THE COURTYARD

>**Date:**
>Friday and Saturday 7:00 p.m.
>**Location:**
>Books & Books in Coral Gables,
>265 Aragon Ave
>Coral Gables, FL 33134
>[Map](https://goo.gl/maps/2XgRch3XXYS2)

The Café’s Wine Bar, located at the entrance to the courtyard, features craft beer and wines from around the world offered by the glass or by the bottle. A free, live music series showcases musicians of all types and genres, including Jazz, African, Latin, Caribbean and World Beat. Every Friday and Saturday beginning at 7:00pm.

[READ MORE](http://www.thecafeatbooksandbooks.com/events.html)

-------------

![Imgur](https://i.imgur.com/iKalJKn.jpg?1)

## AN ORCHESTRA OF MINORITIES
#### Chigozie Obioma

>**Date:**
>Friday 1-11 7:00 p.m.
>**Location:**
>Books & Books in Coral Gables,
>265 Aragon Ave
>Coral Gables, FL 33134
>[Map](https://goo.gl/maps/2XgRch3XXYS2)

A heart-breaking story about a Nigerian poultry farmer who sacrifices everything to win the woman he loves.

CHIGOZIE OBIOMA was born in 1986 in Akure, Nigeria. His short stories have appeared in the Virginia Quarterly Review and New Madrid. His first novel, The Fishermen, won the 2016 NAACP Image Award and was a Man Booker Prize finalist. He has lived in Nigeria, Cyprus and Turkey, and currently resides in the United States, where he teaches at the University of Nebraska-Lincoln.

[READ MORE](https://booksandbooks.com/event/chigozie-obioma/)

-------------

![Imgur](https://i.imgur.com/p0DgcVX.jpg?1)

## BIG TOY EXTRAVAGANZA

>**Date:**
Saturday, January 12th 10:00 AM - 2:00 PM
>**location**
Coral Gables City Hall
405 Biltmore Way
Coral Gables, FL 33134
[MAP](https://goo.gl/maps/ZgQm5uHgf8k)

View, climb and explore! Free event for the family. Explore the City's super-sized cars, trucks and specialized vehicles. Featuring food trucks, face painting and rides.

[READ MORE](https://www.evensi.us/big-toy-extravaganza-coral-gables-city-hall-405-biltmore/282772744)

---------------

![Imgur](https://i.imgur.com/z8fL4DW.jpg?1)

## CHOPIN FOR All
#### Anna Miernik

>**Date:**
Sunday January 13, 2019 3:00 PM
**Location:**
Granada Presbyterian Church
950 University Drive
Coral Gables, FL 33134
[MAP](https://goo.gl/maps/PvdJJ5e8aL32)


Anna Miernik is joining the Chopin for All Concert Series! As a soloist and a chamber musician, she has already performed in 33 countries on 5 continents. Anna has also won several international and national prizes. In 2013 during her concert tour in Australia, New Zealand, Canada and USA Anna gave 15 recitals, including a concert in the world-famous Sydney Opera House.

[READ MORE](http://www.chopin.org)


---------------

![Imgur](https://i.imgur.com/hmRHltp.jpg?1)

## FEATURED LISTING FOR SALE:
#### 12542 SW 107 Ct Miami, FL 33176

Pine Shore! 4/2 Empire model family home, newer roof 2013, wood & porcelain tile, open feeling, sitting on a 15,000 SQ. FT.

[READ MORE](http://yourhomeincoralgables.com/listings/)

## EWM #1 BROKERAGE IN CORAL GABLES

![Imgur](https://i.imgur.com/KMHHgA0.jpg?1)

## Call Us For your Real Estate needs
### Diana Ordonez Tel: 305-510-4698

<!--more-->
