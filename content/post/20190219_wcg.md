---
title: "Weekend in Coral Gables"
date: 2019-02-20T10:50:05-05:00
draft: false
tags: ["Coral-Gables","Events"]
categories: ["Events"]
author: "Futuresarcade"
autoCollapseToc: false
contentCopyright: ''
---
[![Banner](https://www.weekincoralgables.com/DianaBanner2.png)](http://yourhomeincoralgables.com/)

-------------------------------

![ALBITA](https://i.ytimg.com/vi/pDdXT4bbGP0/hqdefault.jpg)

## ALBITA LIVE IN THE MIAMI DESIGN DISTRICT

>**Date:**
Friday Feb 22nd, 6 p.m. - 9 p.m.
>**LOCATION:**
Palm Court - 140 NE 39th Street

Albita - Grammy & Emmy winning artist ALBITA live in concert! The Miami Design District Performance Series presented by Knight Foundation and produced by Emilio Estefan.

[READ MORE](https://www.miamidesigndistrict.net/event/1460/albita-live-in-the-miami-design-district/)

-----------------------
![Imgur](https://i.imgur.com/LX0HAnC.jpg?1)

## SOUTH BEACH WINE AND FOOD FESTIVAL (SOBEWFF)

>**Dates:**
FEB. 20-24
>**Locations:**
Miami
[**Events**](https://sobewff.org/events/)

Back in Miami for its 18th edition, the Food Network & Cooking Channel South Beach Wine & Food Festival (SOBEWFF) will dazzle your taste buds. Nosh on delicious eats crafted by celebrity chefs and attend glitzy late-night parties at this popular annual festival. The five-day event attracts more than 60,000 people and is hosted by Southern Glazer’s Wine & Spirits and Florida International University. Proceeds benefit FIU’s Chaplin School of Hospitality & Tourism Management. Ticket prices vary by event and sell out fast.

[READ MORE](https://sobewff.org/)

-------------------------------------------

![South Miami Rotary Art Festival](http://southmiamiartfest.org/wp-content/uploads/2018/05/MASTER-Logo-blue-350-date2019.jpg)

## SOUTH MIAMI ROTARY ART FESTIVAL

>**Date:**
FEB. 23-24
>**Location:**
Sunset Drive (SW 72nd Street) between Red Road (SW 57th Avenue) and US 1
Miami, FL 33143
[MAP]()

The 35th annual [South Miami Rotary Art Festival] will take place in Downtown South Miami along picturesque Sunset Drive. Listen to live music, hang out in the beer garden and view works by more than 150 artists. Admission is free.

[READ MORE]((http://southmiamiartfest.org/))


----------------------------------

![Imgur](https://i.imgur.com/iUMLL8J.jpg?1)

## MOVIES IN THE PARK: HOTEL TRANSYLVANIA 3

>**Date:**
February 22, 2019, Starting: 7:00 PM
>**Location:**
The Barnacle Historic State Park
3485 Main Highway
Coconut Grove, FL 33133

Bring your blankets and chairs for an evening of movie watching under the stars along beautiful Biscayne Bay. Enjoy Disney favorites and comedic classics.

[READ MORE](https://coconutgrove.com/winter_events/movies-in-the-park/)


---------------------------------
![Imgur](https://i.imgur.com/axkC6y6.jpg?1))

## GIRALDA PRESENTS - YEAH ITS CARIBBEAN MUSIC

>**Date:**
February 22 - 23, 2019, 05:00 PM to 11:00 PM
>**Location:**
Miami, FL
112 Giralda Av
Coral Gables, 33134
[MAP]

Giralda Presents is a series of 6 events taking place the third weekend of every month starting November 23-24, 2018 to April 2019.

[READ MORE](https://www.miamiandbeaches.com/events/detail/giralda-presents---yeah-man-its-caribbean-music/eccef995-772b-426b-aac4-27321d19246c)

--------------------------------

![Imgur](https://i.imgur.com/J4OG3Sc.jpg?1)

## DANCE FOR PEACE

>**Location:**
Miami-Dade County Auditorium
2901 W. Flagler St.
Miami, FL 33135
>**Date:**
February 23, 2019, 8:00 PM
>**Cost:**$35

Dance for Peace is a unique dance program bringing together dance companies across Florida to dance against hate and violence. Profits from the performance will benefit UNICEF USA in support of the children of the world.

[READ MORE](https://www.miamidadecountyauditorium.org/event/dance-for-peace/)


--------------------

## FEATURED LISTING FOR SALE

![Imgur](https://i.imgur.com/CJRrHgq.jpg?1)

### 9066 SW 73rd Ct #605, Miami, FL 33156
#####$255,000.00

Superb location! Across from Dadeland and Metrorail. Impeccable loft style unit, 13 ft ceilings, SS appliances, granite countertops, Italian cabinetry

[READ MORE](https://www.ewm.com/listings/property/a10621119-9066-sw-73rd-ct-miami)

### Call Us For your Real Estate needs
We give expert guidance for selling or buying Real Estate

### Diana Ordonez-Sanchez
Chairman Club Member
EWM Realtors
Tel: 305-510-4698

<!--more-->
