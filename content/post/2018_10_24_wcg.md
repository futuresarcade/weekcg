---
title: ""
date: 2018-10-24T11:08:48+08:00
lastmod: 2018-10-24T11:08:49+08:00
draft: false
tags: ["Coral-Gables","Events"]
categories: ["Events"]
author: "Futuresarcade"
autoCollapseToc: true
contentCopyright: ''
---


[![Banner](https://www.weekincoralgables.com/DianaBanner2.png)](http://yourhomeincoralgables.com/)


![Imgur](https://i.imgur.com/ADp0RWq.png?1)

## BOOKS AND BOOKS

>**Date:**
>Friday and Saturday 7:00 p.m.
>**Location:**
>Books & Books in Coral Gables,
>265 Aragon Ave
>Coral Gables, FL 33134
>[Map](https://goo.gl/maps/2XgRch3XXYS2)

The Café’s Wine Bar, located at the entrance to the courtyard, features craft beer and wines from around the world offered by the glass or by the bottle. A free, live music series showcases musicians of all types and genres, including Jazz, African, Latin, Caribbean and World Beat. Every Friday and Saturday beginning at 7:00pm.

[READ MORE](http://www.thecafeatbooksandbooks.com/events.html)

-------------------------------

![Imgur](https://i.imgur.com/ynR218h.jpg?1)

## VIERNES CULTURALES /CULTURAL FRIDAY

>**Date:**
>Friday 10-26 7:00 -11:00 p.m.
>**Location:**
>8th Street between 13th – 17th  Avenues
>Miami, FL 33174
>Map
>**Cost:** Free
>http://viernesculturales.org/mainvc/

Viernes Culturales features music and works by local artists and artisans in and around Domino Plaza on Southwest 8th Street and 5th Avenue, and behold contemporary and historic artworks at many fine art galleries along Calle Ocho. Savor a delicious dinner at one of the local restaurants, boasting cuisine from all over the world. Come dance, eat, shop and enjoy a delightful evening in Little Havana.

[READ MORE](http://viernesculturales.org/mainvc/)

-----------------------------

![National Theather Live](http://www.gablescinema.com/media/filmassets/NTL_2018_-_King_Lear_-_INT_Listing_Image_-_Portrait.jpg.500x715_q85_crop-smart.jpg)

## NATIONAL THEATER LIVE –KING LEAR

>Date:
Friday 10-26 through Sunday 10-28  @ 12:00 pm
>Location:
Gables Cinema
260 Aragon Ave
Coral Gables, FL 33134-5008
Cost: $20
>[Map](https://www.google.com/maps/place/Coral+Gables+Art+Cinema/@25.7503268,-80.2620697,17z/data=!3m1!4b1!4m5!3m4!1s0x88d9b799f8f757db:0x239eb89da7691c54!8m2!3d25.750322!4d-80.259881)

Broadcast live from London’s West End, see Ian McKellen’s ‘extraordinarily moving portrayal’ (Independent) of King Lear in cinemas. Chichester Festival Theatre’s production received five-star reviews for its sell-out run, and transfers to the West End for a limited season.

[READ MORE](http://www.gablescinema.com/events/king-lear/)

-----------------


![Imgur](https://i.imgur.com/D79AK8e.jpg?1)

## 2 PIANOS & VOICES
#### Beethoven 9th Past & Present

>**Date:** Sunday Oct. 28, 3:00 p.m.
**Location:**
Wertheim Performing Arts Center
10910 SW 17th St.
Miami
**Cost:** $35
[MAP](https://goo.gl/maps/xsSYugt6AGA2)

Renowned Piano Duo Chipak & Kushnir will perform Beethoven’s 9th Symphony as transcribed for 2 pianos by Franz Liszt with local and national singers performing Beethoven’s most famous ‘Ode to Joy’ the rest of the program will be work inspired by Beethoven including a world premier of Robert Chumbley’s “Whisper and Cries” based on Beethoven’s ongoing descent into deafness.

[READ MORE](http://web-extract.constantcontact.com/v1/social_annotation?permalink_uri=2O1ZzSN&image_url=http%3A%2F%2Ffiles.constantcontact.com%2F794229eb001%2F06f7431a-1f9b-47e3-9fd7-8980be539e0d.png)

---------------------


![Imgur](https://i.imgur.com/ZMfrOS6.png?1)

## ADMISSIONS

>**Date:**
Saturday October 13 - November 11
[Tickets](https://web.ovationtix.com/trs/cal/473)
>**Location:**
Gables Stage
1200 Anastasia Avenue
Coral Gables, Florida 33134
[Map](https://goo.gl/maps/5hx1vvHFhQy)
**Cost:** $55

The head of the admissions department at a New England prep school is fighting to diversify the student body. With her husband, the school’s Headmaster, they’ve brought a stodgy institution into the 21st century. When their only son sets his sights on an Ivy League university, personal ambition collides with progressive values! A play that explodes the ideals and contradictions of liberal white America. By the author of BAD JEWS.
“ASTONISHING AND DARING! An extraordinarily useful and excruciating satire – of the left, by the left, for the left – for today!” – New York Times

[READ MORE](http://www.gablestage.org/2017-2018-season/)

--------------------


![Imgur](https://i.imgur.com/ZQqoyeN.jpg?1)

## HAVANA MUSIC HALL

>**Date:**
>October 10 - November 18th, 2018
>[Tickets](https://tickets.actorsplayhouse.org/TheatreManager/1/online)
>**Location:**
>Actor’s Play House
>280 Miracle Mile, Coral Gables, FL 33134
>www.actorsplayhouse.org
>**Cost:** $40
[MAP](https://goo.gl/maps/qLsjCYiSAFp)

This brand new musical was selected specifically by our artistic team to develop in Miami on its way to Broadway. An exciting world premiere, Havana Music Hall reflects the pulse of our community with its irresistible musical beat and heartwarming story centering on Cuban musicians Rolando and Ramona Calderon, and their lives in Cuba from 1958 to the present. Just as their local Havana band is about to get its big break, the Revolution tears apart the only world the duo have ever known. This moving and relevant new musical explores the challenges and joys of one family’s 50-year odyssey for survival, presenting a journey which immigrants of all cultures from around the world have traveled for generations.

[READ MORE](https://www.actorsplayhouse.org/)

---------------------

![Imgur](https://i.imgur.com/7jes8MJ.jpg?3)


## FARMERS MARKET

>**Date:**
>Sunday Year Round 11:00 am – 5:00 pm
>**Location:**
>Shops at Merrick Village
>358 San Lorenzo Ave
>Coral Gables, FL 33146
>[MAP](https://goo.gl/maps/VAzZoWGLfCs)

The Farmers’ Market is located between Tourneau and Nordstrom and takes place from 11 AM – 5 PM every Sunday, year round. We look forward to seeing you at the market.

[READ MORE](https://www.shopsatmerrickpark.com/en/events/the-farmers-market-at-merrick-park-18504.html)

---------------

![Imgur](https://i.imgur.com/Ms4RxMJ.jpg?1)

## HOLLOWEEN ON THE MILE

**Date:**
Wednesday 10-31, 3:30 p.m.
**Location:**
Down Town Coral Gables
260 Aragon Ave
Coral Gables, FL 33134

Trick or Treat! Kids of all ages are invited to Downtown Coral Gables to celebrate the 21st Annual Halloween on the Mile, produced by the Business Improvement District (BID) of Coral Gables on Wednesday, October 31st.

[READ MORE](http://www.shopcoralgables.com/event/21st-annual-halloween-on-the-mile/)

---------------

[![Banner](https://www.weekincoralgables.com/DianaBanner2.png)](http://yourhomeincoralgables.com/)

![Imgur](https://i.imgur.com/p1Ph0rj.jpg?1)

## FEATURED LISTING FOR RENT $2700

### - Just Reduced -
### Furnished or Unfurnished

### 2030 S. Douglas Rd. #423 Coral Gables

#### Live in Down Town Coral Gables

Two bedrooms, two bathrooms, den and big terrace.  Its central location & close proximity to Miracle Mile allows you to walk or take the trolley to a great selection of the best restaurants, café’s, shops, fine art galleries & more, close to public transportation.


[READ MORE](http://yourhomeincoralgables.com/listings/)

## EWM #1 BROKERAGE IN CORAL GABLES

![Imgur](https://i.imgur.com/KMHHgA0.jpg?1)

## Call Us For your Real Estate needs
### Diana Ordonez Tel: 305-510-4698

--------------
