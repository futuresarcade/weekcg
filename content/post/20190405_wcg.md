---
title: "20190405_wcg"
date: 2019-04-02T09:49:00-04:00
draft: false
tags: ["Coral-Gables","Events"]
categories: ["Events"]
author: "Futuresarcade"
autoCollapseToc: false
contentCopyright: ''
---

[![Banner](https://www.weekincoralgables.com/DianaBanner2.png)](http://yourhomeincoralgables.com/)

-------------------
![Imgur](https://i.imgur.com/iHnOFjJ.png?1 =300x)

## Coral Gables Gallery Night Live!
#### Between the Lines of Nature

>**Date:**
March 5 @ 6:00 pm - 11:00 pm,
**Location:**
Coral Gables Museum
285 Aragon Avenue
Coral Gables, FL 33134
[MAP](https://goo.gl/maps/XknzN26PMAK2)

oin us for the Opening Reception of the exhibition “Between the Lines of Nature”; Tina Salvesen and Elisa Albo at the Frank Lynn Gallery – a conversation between artist’s books and poetry. In this occasion we will also premiere The Portfolio Review Series in the Community Meeting Room, where a group of artists has been invited to bring their portfolios and discuss their creative processes and latest ideas with the community.


[READ MORE](http://coralgablesmuseum.org/event/mercedes-benz-of-coral-gables-gallery-night-live-9-2018-11-02/2019-04-05/)

---------------

![Imgur](https://i.imgur.com/xDTREMH.jpg?1 =300x)

## Coral Gables Gallery Night
#### Rob Haff with “Wynwood Black”

>**Date:**
March 5 @ 7:00 pm -10:00 pm,
**Location:**
Books & Books in Coral Gables
265 Aragon Ave
Coral Gables, FL 33134
[MAP](https://goo.gl/maps/XknzN26PMAK2)

Join us for a special Gallery Night followed by a book talk and signing with Manny Hernandez! Throughout the last 29 years, photographer Manny Hernandez has been capturing Miami’s essence in arts, fashion, lifestyle, celebrities, and pop culture. CANDIDS Miami, published by boutique publishing house Wynwood Books, was presented at this past year Miami Book Fair. “My self-published book has been a summer project, that after a few obstacles, it’s finally here” Hernandez said, “These are a selection of some…

[READ MORE](https://booksandbooks.com/event/coral-gables-gallery-night-featuring-rob-haff-with-wynwood-black/)

----------------

![Imgur](https://i.imgur.com/RsfSDwJ.jpg?1 =300x)

## Giralda Under the Stars

>**Date:**
Now - April 5, 201,
**Location:**
Giralda Plaza
Coral Gables, FL 33134-5900
[MAP](https://goo.gl/maps/t9rDkeRmHT12)

Every first Friday of the month from November 2018 – April 2019, the community promenade will come alive with outdoor dining and music by local bands. Make sure to come early and fill those bellies with the delicious local eats of Restaurant Row’s vibrant selection of eateries. Enjoy samples of Zacapa Rum + Tanqueray Gin courtesy of the event sponsor Diageo. Additionally, WeWork Ponce de Leon will have a Pop-Up Lounge where patrons can kick back, relax and speak with one of its members about their location in Coral Gables.

[READ MORE](https://www.shopcoralgables.com/event/giralda-under-the-stars/)

---------------

![Imgur](https://i.imgur.com/eJDUOpw.jpg?1 =300x)

## Coral Gables Seafood & Music Festival

>**Date:**
April 6 - 7 , 2019
>**Location:**
Ponce Circle Park
2800 Ponce de Leon Blvd.
Coral Gables, Fl. 33134
[MAP](https://goo.gl/maps/GNFvcU8GsB12)

Featuring more than 100 arts, crafts, and, food and drink vendors. Enjoy live music all day on Saturday and Sunday. Bring your best beach chairs, pets (on a leash of course), and hang out in the warm sun. With 5 bands per day there is plenty of music to enjoy.

[READ MORE](https://www.floridatravellife.com/falls-festival-arts)


------------------

![THE FAIR](https://fairexpo.s3.amazonaws.com/images/logo-no-ribbon.png =300x)

## MIAMI-DADE COUNTY FAIR & EXPO:

>**Date:**
MARCH 14 - APRIL 7,
>**Location:**
Miami-Dade County Fair & Exposition
10901 SW 24th St,
Miami, FL 33165
[MAP](https://goo.gl/maps/MaVdgLETdjR2)


The Miami-Dade County Fair & Expo hosts a variety of family-friendly indoor and outdoor events, including the Youth Fair. It's the largest fair in Florida and includes more than 90 carnival rides, livestock and agricultural competitions, live concerts and more. Tickets start at $12.

[READ MORE](https://www.thefair.me/)

------------------

![BIG BUNNY INVASION](https://www.pinecrestgardens.org/Home/ShowPublishedImage/5905/636688018206300000 =300x)
>**Date:**
April 5th - April 20th,
>**Location:** Pinecrest Gardens, 11000 Red Rd, Pinecrest, FL 33156, [MAP](https://goo.gl/maps/dzVAfyyVYxp), **Cost:**$5

This April, Pinecrest Gardens is tossing out the traditional “hunt” and replacing it with relaxineg weekend evenings where both children and adults can play. Edgy and ephemeral, Big Bunny Invasion is a public light-art installation that blends fine art and community to ignite imaginations while providing the perfect setting for families to unwind, create and learn.

[READ MORE](https://www.pinecrestgardens.org/entertainment/events-festivals/eggstravaganza)

-------------------------

![National Theater Live](http://www.gablescinema.com/media/filmassets/NTL_2019_-_The_Tragedy_of_King_Richard_the_Second_-_Website_Listings_Image_Landscape.jpg.460x287_q85_crop-smart.jpg =300x)

## The Tragedy of King Richard the Second
#### National Theater live

>**Date:**
Friday, 4/5 to Sunday, 4/7, 12:00 pm,
**Location:** Gables Cinema, 260 Aragon Ave, Coral Gables, FL 33134, [MAP](https://goo.gl/maps/A4s1cECHe5H2), **Cost:**$20

Simon Russell Beale is Shakespeare's tragic king in this visceral, new production from the Almeida Theatre.

[READ MORE](http://www.gablescinema.com/events/the-tragedy-of-king-richard-the-second/)

--------------

![MIAMI POETRY](https://static1.squarespace.com/static/51acf210e4b09715f92c4566/t/5c8c497a8165f563526c15a1/1552697731584/edited.jpg?format=1500w =300x)

## O, MIAMI POETRY FESTIVAL

>**Date:**
APRIL 1 - 30
>**Location:**
Throughout Miami

April is your chance to encounter poetry in lots of different formats throughout Miami. Each spring, the O, Miami Poetry Festival attempts to deliver a poem to every single person in Miami-Dade County. Sponsored by the John S. and James L. Knight Foundation, the festival features more than 40 free events and 20 projects throughout the city. Most events are free.

[READ MORE](http://www.omiami.org/festival/)

-------------

![Imgur](https://i.imgur.com/YAl5B5o.jpg?1 =300x)
## MIAMI RIVERDAY FESTIVAL

>**Date:**
APRIL 6,
>**Location:**
250 NW North River Dr.,
Miami, FL 33128,
[MAP](https://goo.gl/maps/NebVzkEsVnn)

Cruise down the Miami River on free boat rides, enjoy live music performances and learn more about the Lummus Park Historic District at the Miami Riverday Festival. Event admission is free.

[READ MORE](http://www.miamirivercommission.org/RiverdayFestival.html)

-------------

![BLUES & BARBECUE](https://redlandfruitandspice.com/wp-content/uploads/2015/08/bbq.jpg =300x)

## REDLAND BLUES & BARBECUE FESTIVAL

>**Date:**
APRIL 6 - 7
>**Location:**
24801 SW 187th Ave, Homestead, FL 33031,
[MAP](https://goo.gl/maps/Nz6dbyzcY9q)

Sing the blues and chow down on traditional slow-cooked barbecue at the Redland Blues & Barbecue Festival at the Fruit & Spice Park in Homestead. Family-friendly activities include pony rides and games for the kids. General admission is $8 per person and children 11 and under are free.

[READ MORE](http://redlandfruitandspice.com/event/blues-bbq/all/)

----a----------

![REFI MORTGAGES](https://s13.therealdeal.com/trd/up/2019/03/1200-mortgage-refinancing-going-up.jpg =300x)

## Homeowners are rushing to refi mortgages as rates drop

[READ MORE](https://therealdeal.com/2019/03/27/debt-investors-turn-to-treasury-bonds-for-revenue-to-offset-surge-in-home-loan-refinancings/)
