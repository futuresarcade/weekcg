---
title: "20191010_wcg"
date: 2019-10-10T09:56:24-04:00
draft: false
tags: ["Coral-Gables","Events"]
categories: ["Events"]
author: "Futuresarcade"
autoCollapseToc: false
contentCopyright: ''
---

<head>
    <style type="text/css">
      table {
        border:2px solid #552448;
        margin-top:10px;
        margin-bottom:50px;
      }
      article {
        clear: left;
      }
      img {
        width: 300px;
      }
    </style>
  </head>
<body>
<table width="700">
<tr valign="middle">
<td rowspan="2" width="220"  bgcolor= "#652f56" style="border-right: #552448 1px solid;">
<img src="https://i.imgur.com/r3dTAc6.png" title="source: imgur.com" width="250" alt="r3dTAc6.png">
</td>
<td rowspan="2" width="10">
</td>
<td>
<font style="color: #552448; font-family: Georgia; font-size: .95em"><strong>Diana Ordonez-Sanchez</strong></font><br />
<font style="color: #939598; font-family: Arial; font-size: .85em">Realtor Associate</font><br />
<font style="color: #939598; font-family: Arial; font-size: .85em">Chairman's Club</font><br />
<font style="color: #939598; font-family: Arial; font-size: .85em"><a href="tel:1-305-510-4698">1-305-510-4698</a> </font><br />
<font style="color: #939598; font-family: Arial; font-size: .85em">Berkshire Hathaway HomeServices EWM Realty | Realty Division</font><br />
</td>
</tr>
<tr>
<td height="40" valign="bottom">
<font style="color: #939598; font-family: Arial; font-size: .85em"><span style="color: #552448;">O:</span> 305-960-2400 |<span style="color: #552448;">E:</span> dianaiordonez@gmail.com | <a href="http://www.dianaordonez.com" style="text-decoration: none; color: #552448;">www.dianaordonez.com</a></font><br />
<font style="color: #939598; font-family: Arial; font-size: .85em">550 S. Dixie Hwy, Coral Gables, Florida 33146</font><br />
</td>
</tr>
</table>

</body>

---------------------------


![OKTOBERFEST](http://gascmiami.org/resources/Pictures/Oktoberfest2019%20Flyer.jpg)

## OKTOBERFEST MIAMI

>**Date:** \
OCT. 11-13 & OCT. 18-20 \
**location:** \
German American Club \
11919 SW 56th St, Miami, FL 33175 \
[MAP](https://goo.gl/maps/4X8vg77iq5tKJ9Mc8) \
**Cost:**$10


Enjoy this annual German tradition featuring food, games, and entertainment for the whole family, organized by the German International Parent Association. It's a family-friendly Oktoberfest, featuring carnival rides, game booths with prizes, German food and drink, delicious baked goods, entertainment, activities for kids and more.

[Read More](http://gascmiami.org/oktoberfestmiami)

---------------------------

![Imgur](https://i.imgur.com/opn5Bxu.jpg)

## GROVETOBERFEST

>**Date:** \
OCT. 12 \
**location:** \
Regatta Park \
3500 Pan American Dr, Miami, FL 33133 \
[MAP](https://goo.gl/maps/WTUs9xGEcZZfsRaa6) \
**Cost:**$55 -99

Check out the largest craft brew festival in Florida. Happening at Regatta Park in Coconut Grove, Grovetoberfest offers festival-goers the opportunity for unlimited samples from a variety of 400 local and regional brews, plus exotic and rare varieties.

[Read More](https://www.grovetoberfest.com/)

----------------------------------

![OKTOBERFEST CORAL GABLES](https://img1.wsimg.com/isteam/ip/c1ce9445-a097-4e1c-93f9-af44174178d3/03a7e420-db51-4919-858a-abcdd23797b8.jpg/:/cr=t:0%25,l:0%25,w:100%25,h:100%25/rs=h:1000,cg:true)

## CORAL GABLES OKTOBERFEST

>**Date:** \
October 3 to 13 \
**location:** \
 Fritz and Franz Bierhaus, \
 60 Merrick Way
 Coral Gables, FL 33134 \
 [MAP](https://goo.gl/maps/MQjFSMcTZsedbYSZ8)


Oktoberfest in Coral Gables presents the 25th anniversary of their 10-day beer, food, and music festival with plaza entertainment.

[Read More](https://myoktoberfestmiami.com)

-----------------

![LUMINOSA](https://www.jungleisland.com/luminosa-chinese-lantern-festival/wp-content/uploads/2019/10/luminosa-chinese-lantern-festival-1.jpg)

## LUMINOSA! CHINESE LANTERN FESTIVAL

>**Date:** \
OCT. 5-JAN. 8 \
**location:** \
Jungle Island \
1111 Parrot Jungle Trail, Miami, FL 33132 \
[MAP](https://goo.gl/maps/qFAc9Cd6UzfdDRZ56) \
**Cost:**$30.80

Explore a whole new side to this family-friendly attraction, with a magical nighttime journey through the jungle, strewn with hand-crafted Chinese lanterns. These colorful lanterns will be your guide, taking you and your whole family through an exploration of wildlife, birds, and blooms in honor of Miami's lush natural environment starting with beautiful Biscayne Bay. Wind your way through an underwater tapestry filled with fish, turtles and manatees, culminating in a visit to a very "Instagrammable" South Beach, filled with the music and food and imagery that is iconic to the area. This is a truly magical event you won't want to miss.

----------------------

![MIAMI ENTERTAINMENT MONTH](https://www.arshtcenter.org/Global/Subscriptions/Broadway%20in%20Miami%202019-20/BIM-1920-Subs-690x310-v3.jpg)

## MIAMI ENTERTAINMENT MONTHS

>**Date:** \
October 1, 2019 - November 30, 2019 \

October kicks of the two-month long Miami Entertainment Months. You’ll enjoy promotions on everything from craft brewery tastings to movie screenings at state-of-the-art cinemas. Miami’s entertainment scene runs the gamut from concerts to the theater, dance, nightlife and shopping.

[Read More](https://www.miamiandbeaches.com/offers/temptations/miami-entertainment-months)

-----------------------

![MIAMI CARNIVAL](https://s3-us-west-2.amazonaws.com/aws.vbotickets.com/_images/events/30609_event_md.png)

## MIAMI CARNIVAL
>**Date:** \
Sunday OCT.13,  11:00 a.m. - 11:00 p.m. \
**location:** \
Miami Dade-County Fairgrounds \
10901 SW 24th St, Miami, FL 33165 \
[MAP](https://goo.gl/maps/6TpmdFB3xGnzJ9P36) \
**Cost:**$30


Dress in masquerade and celebrate the heritage and traditions of the Caribbean at Miami Carnival. The annual event includes authentic eats, live music, lively parades and contests at various locations across the city.

[Read More](https://miamicarnival.org/)

------------------------

![MOVIE PINECREST](https://www.pinecrestgardens.org/Home/ShowPublishedImage/7319/637024257128670000)

## FAMILY MOVIE NIGHT

>**Date:** \
Friday 10-11, 6:30 p.m. \
**location:** \
Pinecrest Gardens \
11000 Red Rd, Pinecrest, FL 33156 \
[MAP](https://goo.gl/maps/fgwnwTeNmAeQNcGr5) \
**Cost:**$5

The Addams Family (1991)
Themed Halloween activities will be offered from 6:30  p.m. to 7:30 p.m. Costume Contest Dress like an Addams for a chance to win a free Birthday Party at the Picnic Tables.

[Read More](https://www.pinecrestgardens.org/entertainment/movies)

---------------------------

![Frost Music Live](https://s3.amazonaws.com/ovationtix-public-client-assets/prod/client_id_1811/logo_images/FML-Tickets-3.jpg)

## Mysterious Mountain: Frost Symphony Orchestra

>**Date:** \
SATURDAY, OCTOBER 12, 2019, 7:30 – 9PM \
**location:** \
Gusman Concert Hall \
1314 Miller Dr, Coral Gables, FL 33146 \
[MAP](https://goo.gl/maps/BPYifpsMpBMaiDbp6) \
**Cost:**$25

Alan Hovhaness was one of the most prolific and spiritual 20th Century American composers. His Symphony No.2, Mysterious Mountain, anticipated the work of “Holy Minimalists,” such as Arvo Pärt and John Taverner. Frost faculty artists Charles Castleman and Ross Harbaugh join the orchestra for Brahms’s Double Concerto in A Minor. The closing piece is Dmitri Shostakovich’s powerful Symphony No.10 in E Minor, often described as his depiction of Joseph Stalin and the Stalin years.

--------------

![Farmers Market](https://d36ki7ut62e74z.cloudfront.net/prod/optimized/b170_farmers_market_sign.jpg)

## THE MARKET AT MERRICK

Farmer's Market

>**Date:** \
 Sundays 11 a.m. to 6 p.m. \
>**Location:** \
358 San Lorenzo Ave, Coral Gables, FL 33146 \
[MAP](https://goo.gl/maps/zRXV4KAWXJBouUu68)

Visit our fabulous Farmers.Stop by and pick up some beautiful and tasty produce, healthy homemade products, dessert and more! The Farmers' Market is located between Tourneau and Nordstrom

[READ MORE](https://www.shopsatmerrickpark.com/en/events/the-farmers-market-at-merrick-park-18504.html)

-----------------------

## FEATURED LISTINGS FOR SALE

[![Imgur](https://i.imgur.com/pDwNZ0m.png)](http://yourhomeincoralgables.com/listings/)
