---
title: ""
date: 2018-10-31T17:06:09-04:00
draft: false
tags: ["Coral-Gables","Events"]
categories: ["Events"]
author: "Futuresarcade"
autoCollapseToc: true
contentCopyright: ''
---
[![Banner](https://www.weekincoralgables.com/DianaBanner2.png)](http://yourhomeincoralgables.com/)

------------------------

![Imgur](https://i.imgur.com/N2nXgyo.png?1)

## MIAMI INT'L CHILDREN'S FILM FESTIVAL

**Date:** Friday Nov. 2nd to Sunday Nov 4th
**Location:**
Gables Cinema
260 Aragon Ave
Coral Gables, FL 33134-5008
Cost: $20
>[Map](https://www.google.com/maps/place/Coral+Gables+Art+Cinema/@25.7503268,-80.2620697,17z/data=!3m1!4b1!4m5!3m4!1s0x88d9b799f8f757db:0x239eb89da7691c54!8m2!3d25.750322!4d-80.259881)


The Miami International Children’s Film Festival returns this November in partnership with the prestigious New York International Children’s Film Festival!  Culturally rich, entertaining, and inexpensive family fun, the Festival returns for three days of highly-acclaimed international cinema for children and families, featuring several Florida and Miami premieres.  The Festival offers a unique opportunity to cultivate critical appreciation for intelligent films among younger people, while educating and entertaining. As with years past, the Festival also offers Family fun on the Plaza, Opening Night Reception party and more!

[READ MORE](http://www.gablescinema.com/programs/micff-2018/)

----------------

![Imgur](https://i.imgur.com/wBwRiIR.jpg?2)

## CORAL GABLES GALLERY NIGHT

> **Date:**
>Friday 11-2 6:30 pm – 10:00 pm
>**Location:**
>Various galleries
>Coral Gables, Florida
>**Cost:** Free
>[Gallery Night Trolley](http://www.citybeautiful.net/Modules/ShowDocument.aspx?documentID=23109)

Held the first Friday of the month, Gallery Nights showcase the various works from European and Latin American art masters to outstanding contemporary artists. Be on the lookout for the orange paintbrush on the side of the Trolley and on special Trolley stop signs to find a dedicated Gallery Night Trolley that will loop between Coral Gables galleries and art venues participating on Gallery Night.

-------------------------

![Imgur](https://i.imgur.com/ADp0RWq.png?1)

## Evan J. Alexander - Abstract Coordinates
#### Coral Gables Gallery Night

>**Date:**
>Friday Nov. 2nd 7:00 P.M
>**Location:**
>Books & Books in Coral Gables,
>265 Aragon Ave
>Coral Gables, FL 33134
>[Map](https://goo.gl/maps/2XgRch3XXYS2)

Abstract Coordinates 30.40, features the work of Florida native, Evan J. Alexander. Comprised of original oil and acrylic paintings the art on display was inspired by the painterly quality of satellite imagery. Evan has focused on a controlled form of abstraction in the paintings made for this exhibition. He navigated certain landscapes to find compositions that aligned with the conventions of his prior work. This furthers the perpetual incorporation of advanced technologies within the traditional medium of oil…

[READ MORE](https://booksandbooks.com/event/coral-gables-gallery-night-opening-evan-j-alexander/)

------------------------------------

![Imgur](https://i.imgur.com/CJASb6H.jpg?1)

## RENATO MEZIAT
#### A Decade of Realism

>**Date:**Friday Nov. 2nd, 6 to 10 pm on Friday,
Gables Gallery Night
**location:**
ArtSpace Virginia Miller Galleries
169 Madeira Avenue
Coral Gables, FL 33134

Over the past 44 years, I’ve had the privilege of exhibiting a wildly diverse panoply of modern and contemporary as well as traditional ethnic art, ranging from historically significant to provocative, conceptual works.---Please join us on Friday, Nov. 2nd from 6:00 - 10:00 pm to meet Renato Meziat and his lovely daughter, Luiza, who is a model in some of his figurative paintings.

[READ MORE](http://www.virginiamiller.com/?utm_source=Virginia%20Miller%20Mailing%20List&utm_campaign=4c0bcbd112-EMAIL_CAMPAIGN_6_COPY_01&utm_medium=email&utm_term=0_60ae927881-4c0bcbd112-581900653)

---------------------

![Imgur](https://i.imgur.com/ylhCyfR.jpg?1)

## MERCEDES-BENZ - Gallery Night Live!
### KINDRED SPIRITS

> **Date:**
>Friday 10-5, 6:30 pm-10 pm
>**Location:**
>Coral Gables Museum
>285 Aragon Avenue
>Coral Gables, FL 33134
>**Cost:** Free
[MAP](https://www.google.com/maps/place/Coral+Gables+Museum/@25.7505925,-80.2625289,17z/data=!3m1!4b1!4m5!3m4!1s0x88d9b79a1b3ffaa5:0x9aad47c710d60050!8m2!3d25.7505925!4d-80.2603402)

Join us for Gallery Night Live, sponsored by Mercedes-Benz of Coral Gables!

The Museum will be premiering the exhibitions Kindred Spirits: Ten Artists by the Hudson at the Fewell Gallery, curated by Anelys Alvarez and Me, Merrick and the Songs of the Wind, by artist Jacqui Roch, at the Frank Lynn Gallery.

[READ MORE](http://coralgablesmuseum.org/event/mercedes-benz-of-coral-gables-gallery-night-live-9-2018-11-02/2018-11-02/)

--------------------------

![Imgur](https://i.imgur.com/wIVsZw0.jpg?1)

## Metropolitan Opera Orchestra

**Date:**
Friday 11-2, 8:00 P>M>
**Location:**
First Miami Presbyterian Church
609 Brickell Avenue
Miami, 33131.
**Tickets starting at** $ 10.

Miami Virtuosi invites the well-known Metropolitan Opera Orchestra to a musical and intimate encounter, where the acclaimed flutist Trudy Kane and the award-winning pianist Oleksii Ivanchenco will perform, performing compositions by Gaubert, Martinu, Burton, Zyman and Tchaikovsky.

[READ MORE](https://www.facebook.com/events/313205962795023/)


---------------------------

![Imgur](https://i.imgur.com/8XAwuzR.jpg?2)

## FIU Music Festival: Music for Two Pianos. FIU-Wertheim.

>**Date:** Sunday Nov.2nd, 7:30 p.m.
**Location:**
Wertheim Performing Arts Center
10910 SW 17th St.
Miami
**Cost:** $15
[MAP](https://goo.gl/maps/xsSYugt6AGA2)

A chamber music performance for the younger generation! FIU’s Artist-in-Residence, the Amernet String Quartet, will be joined by Juilliard clarinetist, Jon Manasse. The evening opens with a solo clarinet work by Paquito D’Rivera (Lecuonerias), followed by Debussy’s String Quartet in G-minor.

[READ MORE](http://carta.fiu.edu/music/event/fiu-music-festival-amernet-string-quartet-clarinetist-jon-manasse/)


--------------------------

![Imgur](https://i.imgur.com/z8fL4DW.jpg?1)

## Chopin for All.
#### Elliot Wuu. Granada.

>**Date:**
Sunday Nov. 4th,  3 PM
**Location:**
Granada Presbyterian Church
950 University Drive,
Coral Gables,

ELLIOT WUU
Hilton Head International Piano Competition
winner and Gilmore Young Pianist

[READ MORE](http://www.chopin.org/events.html)

--------------------------

![Imgur](https://i.imgur.com/ZMfrOS6.png?1)

## ADMISSIONS

>**Date:**
Saturday October 13 - November 11
[Tickets](https://web.ovationtix.com/trs/cal/473)
>**Location:**
Gables Stage
1200 Anastasia Avenue
Coral Gables, Florida 33134
[Map](https://goo.gl/maps/5hx1vvHFhQy)
**Cost:** $55

The head of the admissions department at a New England prep school is fighting to diversify the student body. With her husband, the school’s Headmaster, they’ve brought a stodgy institution into the 21st century. When their only son sets his sights on an Ivy League university, personal ambition collides with progressive values! A play that explodes the ideals and contradictions of liberal white America. By the author of BAD JEWS.
“ASTONISHING AND DARING! An extraordinarily useful and excruciating satire – of the left, by the left, for the left – for today!” – New York Times

[READ MORE](http://www.gablestage.org/2017-2018-season/)

-------------------------


![Imgur](https://i.imgur.com/ZQqoyeN.jpg?1)

## HAVANA MUSIC HALL

>**Date:**
>October 10 - November 18th, 2018
>[Tickets](https://tickets.actorsplayhouse.org/TheatreManager/1/online)
>**Location:**
>Actor’s Play House
>280 Miracle Mile, Coral Gables, FL 33134
>www.actorsplayhouse.org
>**Cost:** $40
[MAP](https://goo.gl/maps/qLsjCYiSAFp)

This brand new musical was selected specifically by our artistic team to develop in Miami on its way to Broadway. An exciting world premiere, Havana Music Hall reflects the pulse of our community with its irresistible musical beat and heartwarming story centering on Cuban musicians Rolando and Ramona Calderon, and their lives in Cuba from 1958 to the present. Just as their local Havana band is about to get its big break, the Revolution tears apart the only world the duo have ever known. This moving and relevant new musical explores the challenges and joys of one family’s 50-year odyssey for survival, presenting a journey which immigrants of all cultures from around the world have traveled for generations.

[READ MORE](https://www.actorsplayhouse.org/)

--------------------------

![Imgur](https://i.imgur.com/hmRHltp.jpg?1)

## FEATURED LISTING FOR SALE:
#### 12542 SW 107 Ct Miami, FL 33176

Pine Shore! 4/2 Empire model family home, newer roof 2013, wood & porcelain tile, open feeling, sitting on a 15,000 SQ. FT.

[READ MORE](http://yourhomeincoralgables.com/listings/12542-sw-107-ct-miami-fl-33176/)

[READ MORE](http://yourhomeincoralgables.com/listings/)

## EWM #1 BROKERAGE IN CORAL GABLES

![Imgur](https://i.imgur.com/KMHHgA0.jpg?1)

## Call Us For your Real Estate needs
### Diana Ordonez Tel: 305-510-4698

-------------------------
