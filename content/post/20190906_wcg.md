---
title: "20190906_wcg"
date: 2019-09-05T10:33:03-04:00
draft: false
tags: ["Coral-Gables","Events"]
categories: ["Events"]
author: "Futuresarcade"
autoCollapseToc: false
contentCopyright: ''
---

<head>
    <style type="text/css">
      table {
        border:2px solid #552448;
        margin-top:10px;
        margin-bottom:50px;
      }
      article {
        clear: left;
      }
      img {
        width: 300px;
      }
    </style>
  </head>
<body>
<table width="700">
<tr valign="middle">
<td rowspan="2" width="220"  bgcolor= "#652f56" style="border-right: #552448 1px solid;">
<img src="https://i.imgur.com/r3dTAc6.png" title="source: imgur.com" width="250" alt="r3dTAc6.png">
</td>
<td rowspan="2" width="10">
</td>
<td>
<font style="color: #552448; font-family: Georgia; font-size: .95em"><strong>Diana Ordonez-Sanchez</strong></font><br />
<font style="color: #939598; font-family: Arial; font-size: .85em">Realtor Associate</font><br />
<font style="color: #939598; font-family: Arial; font-size: .85em">Chairman's Club</font><br />
<font style="color: #939598; font-family: Arial; font-size: .85em"><a href="tel:1-305-510-4698">1-305-510-4698</a> </font><br />
<font style="color: #939598; font-family: Arial; font-size: .85em">Berkshire Hathaway HomeServices EWM Realty | Realty Division</font><br />
</td>
</tr>
<tr>
<td height="40" valign="bottom">
<font style="color: #939598; font-family: Arial; font-size: .85em"><span style="color: #552448;">O:</span> 305-960-2400 |<span style="color: #552448;">E:</span> dianaiordonez@gmail.com | <a href="http://www.dianaordonez.com" style="text-decoration: none; color: #552448;">www.dianaordonez.com</a></font><br />
<font style="color: #939598; font-family: Arial; font-size: .85em">550 S. Dixie Hwy, Coral Gables, Florida 33146</font><br />
</td>
</tr>
</table>

</body>

----------------------------

![Imgur](https://i.imgur.com/sBfDw0V.jpg?1)

## MIAMI SPICE
>**Date:**  \
AUG. 1 - SEPT. 30 \
>**Location:**  \
[Participating Restaurants](https://www.miamiandbeaches.com/offers/temptations/miami-spice-months/list-at-a-glance)

August kicks off two months of Miami Spice, when restaurants in all corners of the destination offer special three-course, prix-fixe menus featuring their tastiest dishes at marked down prices (lunch/brunch $23, dinner $39). Make a reservation, get out there, and taste, taste, taste!

[READ MORE](https://www.miamiandbeaches.com/offers/temptations/miami-spice-months)

--------------------------

![ARTSLAUNCH](https://www.arshtcenter.org/Global/Plays/2019/ArtsLaunch%202019/ArtsLaunch-733x278.jpg)

## ARTSLAUNCH 2019:

>**Date:** \
SEPTEMBER 7 \
**location:** \
Adrienne Arsht Center for the Performing Arts \
1300 Biscayne Blvd, Miami, FL 33132 \
[MAP](https://goo.gl/maps/YsttianNeevBdy5m6)

Celebrate the hard work of Miami’s non-profit art groups at ArtsLaunch 2019. Held annually at the Adrienne Arsht Center for the Performing Arts, this popular community event features more than 60 performances and activities for the whole family to enjoy – and it’s totally free!

[Read More](https://www.arshtcenter.org/Tickets/Calendar/2019-2020-Season/ArtsLaunch/)

---------------------------------

![FAIR EXPO](https://fairexpo.s3.amazonaws.com/images/logo-no-ribbon.png)

## THE CHILDREN’S TRUST FAMILY EXPO:

>**Date:** \
SEPTEMBER 7 \
**location:** \
Miami-Dade Fair & Expo Center \
10901 SW 24th St, Miami, FL 33165 \
[MAP](https://goo.gl/maps/fUG1xtuLi61Qd1wf7)


For one day, the Miami-Dade Fair & Expo Center transforms into a family extravaganza for more than 15,000 people interested in connecting with health and social services, educational opportunities and family fun. Get connected with after-school activities and nutrition education outlets, and experience a book fair, sports demonstrations, face painting, magicians and more, all for free.

[Read More](https://www.fairexpo.com/expo-upcoming-events.php)

---------------------------

![Books&Books](https://i0.wp.com/i.imgur.com/xDTREMH.jpg?zoom=1.8700001239776611&ssl=1)

## Coral Gables Gallery Night
“Small Worlds” Exhibit: Meet the Artist Reception

>**Date:** \
Friday September 6, 7:00 pm - 10:00 pm \
>**Location:** \
Books & Books in Coral Gables \
265 Aragon Ave \
Coral Gables, FL 33134 United States \
Phone:  3054424408 \
[MAP](https://goo.gl/maps/HUJnszBkKZy)

Books & Books Bookstore in Coral Gables is proud to showcase “Small Worlds,” a stunning exhibit that captures the soul of nature and wildlife as seen through the eyes of acclaimed conservation photographer Phoenix. From the majesty and frailty of the Florida Everglades to the haunting beauty of Ireland, each fine art photograph shares a story of our collective small world. Miami native and Fort Lauderdale resident Phoenix saw within many of her larger, collected works compositions that fit the…


[READ MORE](https://booksandbooks.com/event/small-worlds-exhibit-meet-the-artist-reception/)

-----------------

![Imgur](https://i.imgur.com/EEe4CKN.png?2)

## GALLERY NIGHT LIVE

>**Date:** \
Friday 9-6 @ 7:00 pm - 11:00 pm \
>**Location:** \
Coral Gables Museum, \
285 Aragon Avenue \
Coral Gables, FL 33134 \
[MAP](https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=285+Aragon+Avenue+Coral+Gables+FL+33134+United+States)

Join us on Gallery Night and attend the Grand Opening of the shows The Caribbee Club: Krome Avenue; Paintings by Andres Cabrera at the Join us for the Grand Opening of The ​Caribbee Club: ​Body Landscapes; Designs by Andrea Spiridonakos at the Frank Lynn Gallery. Also on view are ​America Weaves, curated by Adriana Herrera, at the Fewell Gallery; ​The Art of Compassion, curated by Starr Sariego, in Gallery 109 and Abraham Gallery; ​Doble filo: geografías y palabras, curated by Felix Suazo, in the Community Meeting Room; and our permanent exhibition, ​Creating the Dream. Among other amenities in the courtyard, there will be live…

[READ MORE](https://coralgablesmuseum.org/event/mercedes-benz-of-coral-gables-gallery-night-live-9-2018-11-02/2019-09-06/)


---------------------------------------

![Frost Music Live](https://s3.amazonaws.com/ovationtix-public-client-assets/prod/client_id_1811/logo_images/FML-Tickets-3.jpg)]

## FROST MUSIC LIVE
Frost School’s Henry Mancini Institute Orchestra in Concert

>**Date:** \
Friday 9-6, 7:30 p.m. \
**location:** \
UM Gusman Concert Hall \
1314 Miller Drive \
Coral Gables, FL \
[MAP](https://goo.gl/maps/LBkv9McAYsU1T2fd9)

**Cost:**$20

Maria Schneider, artistic director
Scott Flavin, resident conductor

Experience the Florida premiere of Mary Lou Williams’s Zodiac Suite. ---

[Read More](https://ci.ovationtix.com/1811/performance/10439975)

-------------------------------------

![UPS 5K](http://teamfootworks.org/wp-content/uploads/2014/05/UPS-TFW-800x300_c.png)

## 10th Annual UPS 5K Run

>**Date:** \
Saturday, September 7⋅6:00 – 10:00am \
**location:** \
Coral Gables City Hall \
405 Biltmore Way, \
Coral Gables, FL 33134, USA \
[MAP](https://goo.gl/maps/EAswQE2SxEunyEvS9)

Description:The UPS 5K Benefiting the United Way of Miami-Dade takes participants through the beautiful city of Coral Gables past city landmarks such as the Biltmore Hotel. The race still starts and ends at the City of Coral Gables Historic City Hall with a festive post-race party at Merrick Park. The park is across the street from City Hall at the intersection of Le Jeune Road and Miracle Mile.

[Read More](http://teamfootworks.org/the-ups-5k/)

--------------------------------

![MIAMI](https://i.imgur.com/HzFKIHr.jpg)

## CITYLAB: MIAMI RANKS AT TOP OF FASTEST GROWING CITIES LIST IN ‘BIGGEST SURPRISE’

[Read More](https://www.thenextmiami.com/citylab-miami-ranks-at-top-of-fastest-growing-cities-list-in-biggest-surprise/)

--------------------------------

![Imgur](https://i.imgur.com/Tg8lnqZ.jpg)

## BAHAMIAN RELIEF EFFORT

[Read More](https://www.ewm.com/bahamas/)
