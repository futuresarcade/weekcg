---
title: "190614_wcg"
date: 2019-06-12T12:02:37-04:00
draft: false
tags: ["Coral-Gables","Events"]
categories: ["Events"]
author: "Futuresarcade"
autoCollapseToc: false
contentCopyright: ''
---

[![Banner](https://www.weekincoralgables.com/DianaBanner2.png)](http://yourhomeincoralgables.com/)

-------------------


![Restaurant Week](http://restaurantweek.shopcoralgables.com/wp-content/uploads/2019/05/BID_CulinaryMonth_WebsiteBanner_1920x500_05.07.19_Draft1-1920x500.jpg =300x)


## CORAL GABLES RESTAURANT WEEK

>**Date:**  \
JUNE 4 –24 \
>**Location:**  \
[Restaurants](http://restaurantweek.shopcoralgables.com/restaurants/)

The rest of the month of June is filled with colorful cocktails and delicious food specials at restaurants on Miracle Mile and throughout Downtown Coral Gables. Coral Gables Restaurant Week features more than 40 eateries offering special three-course, prix-fixe lunch and dinner menus at discounted prices.

[READ MORE](http://restaurantweek.shopcoralgables.com/)

--------------------

![OUT IN THE TROPIC](http://fundarte.us/wp-content/uploads/2018/05/Ecard-Image-OIT18.jpg =300x)

## Out In The Tropics

>**Date:** \
May 24 – June 24 \
>**Location:** \
Miami Beach

A month of presentations by artists addressing issues ranging from politics to sexuality. The 10th season of Out in the Tropics presented by FUNDarte will feature a variety of performances at venues such as Centro Cultural Español, The Gleason Room at The Fillmore, The Miami Beach Botanical Garden and more. Tickets start at $25.

[READ MORE](http://fundarte.us/fundarte-event/out-in-the-tropics/)

-----------------------


![Imgur](https://i.imgur.com/BAB2LMX.jpg?1)

## CITY THEATRE’S SUMMER SHORTS
>**Date:** \
MAY 30 – JUNE 30 \
>**Location:** \
Carnival Studio Theater \
1300 Biscayne Blvd, \
Miami, FL 33132 \
[MAP](https://goo.gl/maps/hUtk97hPc1CCH7Ub9)

Celebrating its 23rd season of ten-minute entertaining plays, City Theatre’s Summer Shorts, America’s Short Play Festival, is happening throughout the month of June. Tickets start at $39 per person and are available through the Adrienne Arsht Center for the Performing Arts.

[READ MORE](https://www.citytheatre.com/summer-shorts)

-----------------------

![Beachtone Jazz Festival](https://www.miamiartguide.com/wp-content/uploads/2019/06/Beachtone-Jazz-Festival2019.jpg =300x)


## Beachtone Jazz Festival

>**Date:**  \
June 15, 2019 \
From: 07:00 PM \
>**Location:**  \
ADRIENNE ARSHT CENTER \
KNIGHT CONCERT HALL \
1300 Biscayne Blvd. \
Miami, FL 33132 \
[MAP](https://goo.gl/maps/pWoRzCLQn2DrCgbUA) \
>**Cost:**$55

The Beachtone Music Festival presents an unprecedented roster featuring Grammy Award winner Eliane Elias, Brazilian guitarist Yamandu Costa, legendary percussionist Sammy Figueroa and more. The Beachtone Jazz Festival perfectly captures essence of Miami in its prime musical experience.

[READ MORE](https://www.arshtcenter.org/Tickets/Calendar/2018-2019-Season/Summer-Shows/Beachtone/)

-----------------

![Movies at Giralda](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTrElDS10BJNz353Vz863akRp4groA0nIypmytgv0BohH1gA4RA_0XjPMTPOZg =300x)

## Movies on Giralda Plaza

>**Date:**  \
June 15, 2019  8:00 pm - 10:00 pm \
>**Location:**  \
Giralda Plaza, \
Giralda Ave, \
Coral Gables, FL 33134 \
[MAP](https://goo.gl/maps/1bE4yPYqMTyUhDdw9)

The City of Coral Gables, in partnership with the Business Improvement District of Coral Gables and Giralda Plaza, invite you to the movies on the first and third Saturdays of the month.

Please bring your lawn chairs. Free popcorn will be available to complete the experience and several restaurants will be offering to-go boxes that’ll be ready for pick-up before the movie

-----------------

![Deering Estate Father's Day](https://deeringestate.org/wp-content/uploads/2019/04/pelican-skipper-768x420.jpg =300x)

## FATHER’S DAY FISHING CRUISE

>**Date:**  \
Sunday June 16, 11 a.m. & 2 p.m. \
>**Location:**\
Deering Estate, \
16701 SW 72 Ave.\
Miami FL 33157, \
[MAP](https://goo.gl/maps/dgeiaTG5ZBF2) \
**Cost:** $35

Celebrate Father’s Day with a cruise on Biscayne Bay! Throughout the year, Pontoon Boat Tours are offered on the Pelican Island Skipper, a 45-foot Corinthian Catamaran. This trip will depart from Deering Estate at 1:00 PM and return at 4:00 PM for a three hour fishing trip. Deering Estate Naturalists will join to educate guests…

[READ MORE](https://deeringestate.org/event/fathers-day-bay-cruise/)

---------------------

![Father's Day](https://cdn.pixabay.com/photo/2016/03/29/14/57/happy-fathers-day-1288443_1280.png =300x)

## MORE THINGS TO DO ON FATHER'S DAY

[Read More](https://www.miamiandbeaches.com/events/what-to-do-for-fathers-day)

-------------------



![Metropolis](https://i1.wp.com/yourhomeincoralgables.com/wp-content/uploads/2019/02/IMG_3049.jpg?w=400)

## METROPOLIS APARTMENT - FOR SALE
#### 9066 SW 73 Ct # 605 Miami, Florida 33156

Loft style apartment at Dadeland. 13 FT ceilings add to the spacious feeling of this open plan 1 bedroom, 1 bath + den. Metropolis Dadeland, walking distance to Restaurants and shopping. 2 swimming pools, Jacuzzi, fitness center, recreational room 24 hr concierge services and valet parking.

[READ MORE](http://yourhomeincoralgables.com/listings/9066-sw-73-ct-605-miami-florida-33156/)

----------------------
