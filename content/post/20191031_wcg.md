---
title: "20191031_wcg"
date: 2019-10-31T09:58:44-04:00
draft: false
tags: ["Coral-Gables","Events"]
categories: ["Events"]
author: "Futuresarcade"
autoCollapseToc: false
contentCopyright: ''
---

<head>
    <style type="text/css">
      table {
        border:2px solid #552448;
        margin-top:10px;
        margin-bottom:50px;
      }
      article {
        clear: left;
      }
      img {
        width: 300px;
      }
    </style>
  </head>
<body>
<table width="700">
<tr valign="middle">
<td rowspan="2" width="220"  bgcolor= "#652f56" style="border-right: #552448 1px solid;">
<img src="https://i.imgur.com/r3dTAc6.png" title="source: imgur.com" width="250" alt="r3dTAc6.png">
</td>
<td rowspan="2" width="10">
</td>
<td>
<font style="color: #552448; font-family: Georgia; font-size: .95em"><strong>Diana Ordonez-Sanchez</strong></font><br />
<font style="color: #939598; font-family: Arial; font-size: .85em">Realtor Associate</font><br />
<font style="color: #939598; font-family: Arial; font-size: .85em">Chairman's Club</font><br />
<font style="color: #939598; font-family: Arial; font-size: .85em"><a href="tel:1-305-510-4698">1-305-510-4698</a> </font><br />
<font style="color: #939598; font-family: Arial; font-size: .85em">Berkshire Hathaway HomeServices EWM Realty | Realty Division</font><br />
</td>
</tr>
<tr>
<td height="40" valign="bottom">
<font style="color: #939598; font-family: Arial; font-size: .85em"><span style="color: #552448;">O:</span> 305-960-2400 |<span style="color: #552448;">E:</span> dianaiordonez@gmail.com | <a href="http://www.dianaordonez.com" style="text-decoration: none; color: #552448;">www.dianaordonez.com</a></font><br />
<font style="color: #939598; font-family: Arial; font-size: .85em">550 S. Dixie Hwy, Coral Gables, Florida 33146</font><br />
</td>
</tr>
</table>

</body>

---------------------------

![Imgur](https://i.imgur.com/ZEW9lBy.jpg)

## HALLOWEEN

>**Date:** \
OCT. 31\
>**location:** \
Miracle Mile \
Coral Gables \
[MAP](https://goo.gl/maps/ua9vV5wxbBToWcmu6)

Downtown Coral Gables invites kids of all ages to celebrate the annual Halloween on the Mile. Offering a lineup of exciting Halloween activities, Halloween on the Mile is a treat the whole family will enjoy. There will be spooky storytelling, arts andcrafts, a kiddie costume contest and a doggie costOme contest, produced by the Coral Gables Business Improvement District (BID).

[Read More](https://www.facebook.com/events/255625475292106/)

----------------------


![Imgur](https://i.imgur.com/ADp0RWq.png?1)

## GALLERY NIGHT - Books & Books
Carlos Barbón

>**Date:** \
>Friday 11-1 @ 7:00 p.m. \
**Location:** \
Books & Books in Coral Gables \
265 Aragon Ave \
Coral Gables, FL 33134 \
[Map](https://goo.gl/maps/2XgRch3XXYS2)

Carlos Garcia-Barbón is a Cuban-born artist and a graduate of Florida State University with a BA degree in Art and a minor in Art History.  Carlos has been awed by Coral Gables beauty since childhood, with its well-designed plans, wide streets and old world charm. He is dedicated to documenting the city of Coral Gables in watercolors using the same techniques as that of the old masters, but with a updated contemporary approach.  Barbón’s watercolors have been on exhibit in…

[Read More](https://booksandbooks.com/event/gallery-night-carlos-barbon/)

-------------------


![Imgur](https://i.imgur.com/EEe4CKN.png?2)

## GALLERY NIGHT LIVE

>**Date:** \
Friday 11-1 @ 7:00 pm - 11:00 pm \
>**Location:** \
Coral Gables Museum, \
285 Aragon Avenue \
Coral Gables, FL 33134 \
[MAP](https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=285+Aragon+Avenue+Coral+Gables+FL+33134+United+States)

Join us for the Grand Opening of The ​Caribbee Club: ​Body Landscapes; Designs by Andrea Spiridonakos at the Frank Lynn Gallery. Also on view are ​America Weaves, curated by Adriana Herrera, at the Fewell Gallery; Peruvian Beauty, The Photographic Work of Yayo Lopez; and our permanent exhibition, ​Creating the Dream. Among other amenities in the courtyard, there will be live music in collaboration with the University of Miami’s Frost School of Music, a cash bar and light bites. This event…

[Read More](https://coralgablesmuseum.org/event/mercedes-benz-of-coral-gables-gallery-night-live-9-2018-11-02/2019-11-01/)

----------------------------------

![DIA DE LOS MUERTOS](https://shopcoralgables.com/wp-content/uploads/2019/10/INVITACION-DIA-DE-MUERTOS-CONSULADO-MIAMI-ENGLISH-cropped3.jpg)

## DIA DE LOS MUERTOS EN LA PLAZA

>**Date:** \
Saturday, November 2nd 4:00- 7:00 p.m. \
**location:** \
Thompson Plaza for the Arts \
1300 Biscayne Blvd, Miami, FL 33132 \
[MAP](https://goo.gl/maps/f5xRmAFLMwJwHo8a9)

Experience and learn about the Mexican holiday of Día de los Muertos with activities for the whole family!

[Read More](https://www.arshtcenter.org/Tickets/Calendar/2019-2020-Season/Arsht-Center-Presents/Dia-de-los-Muertos-en-La-Plaza/)

--------------------------

## DAY OF THE DEAD CELEBRATION

>**Date:** \
Sunday November 3rd, 2 p.m. - 8 p.m. \
**location:** \
Giralda plaza \
Giralda Ave, Coral Gables, FL 33134 \
[MAP](https://goo.gl/maps/5NrhLz9FopamQSwe9)

The Consulate General of Mexico in Miami and the City of Coral Gables invite you to the First Grand Celebration of the Day of the Dead! Come and enjoy with your family an afternoon of live entertainment, music, and tradition. Join us Sunday, November 3rd, 2019 from 2pm-8pm on Giralda Plaza of Coral Gables. This event is free and so are the memories!

https://www.shopcoralgables.com/event/dia-de-los-muertos/

-----------------------

![Miami Auto Show](https://www.miamiandbeaches.com/getmedia/93c3ef4e-a800-47e0-afb4-6099d3200680/car-in-museum-1440x900.jpg.aspx?width=1440&height=900&ext=.jpg&width=430&mode=max)

## MIAMI INTERNATIONAL AUTO SHOW

>**Date:** \
November 1-10 \
**location:** \
1901 Convention Center Dr, Miami Beach, FL 33139 \
[MAP](https://goo.gl/maps/kq2ZGwP5pWibm8277) \
**Cost:**$15

One of the largest and most prestigious Auto Shows in the nation, is a venue for national product introductions, it showcases more than a thousand new vehicles from over 40 manufacturers from around the world – a collection that auto enthusiasts wait all year to see.

[Read More](http://www.miamiautoshows.com/)

----------------

![Miami Art Festival](https://festivalnet.com/fno/image.php?mode=fno_event_image&band_id=12845&fno_event_id=69698&width=320&height=crop&theme=Sage)

## South Miami Art Festival

>**Date:** \
November 2-3, 10 am -5 pm \
**location:**
The Shops at Sunset Place \
5701 Sunset Dr Suite 350, South Miami, FL 33143 \
[MAP](https://goo.gl/maps/dGbHKqWEtCtWyKm47)

Art and culture lovers can enjoy this free festival taking place in the heart of South Miami nestled between wide sidewalks, cafés, quaint shops and lush landscaping. The annual ChamberSOUTH Miami...

[Read More](https://festivalnet.com/69698/South-Miami-Florida/Art-Shows/ChamberSouth-South-Miami-Juried-Fine-Arts-Festival)

-----------------

![Imgur](https://i.imgur.com/XVRfu3I.png?1)
## Seafood Festival South Dade

>**Date:** \
Saturday at 5 PM – 9 PM \
**location:** \
BrandsMart USA South Dade \
16051 S Dixie Highway, Miami, Florida 33157 \
[MAP](https://goo.gl/maps/HMLdX4Wk43ntLVRz7)

CRABS, SHRIMP, CONCH, FISH, MUSIC AND MUCH MORE

[Read More](https://www.eventbrite.com/e/seafood-festival-south-dade-tickets-66416382187?aff=ebdiglgoogleseo)

------------------------

![LUMINOSA](https://www.jungleisland.com/luminosa-chinese-lantern-festival/wp-content/uploads/2019/10/luminosa-chinese-lantern-festival-1.jpg)

## LUMINOSA! CHINESE LANTERN FESTIVAL

>**Date:** \
OCT. 5-JAN. 8 \
**location:** \
Jungle Island \
1111 Parrot Jungle Trail, Miami, FL 33132 \
[MAP](https://goo.gl/maps/qFAc9Cd6UzfdDRZ56) \
**Cost:**$30.80

Explore a whole new side to this family-friendly attraction, with a magical nighttime journey through the jungle, strewn with hand-crafted Chinese lanterns. These colorful lanterns will be your guide, taking you and your whole family through an exploration of wildlife, birds, and blooms in honor of Miami's lush natural environment starting with beautiful Biscayne Bay. Wind your way through an underwater tapestry filled with fish, turtles and manatees, culminating in a visit to a very "Instagrammable" South Beach, filled with the music and food and imagery that is iconic to the area. This is a truly magical event you won't want to miss.

[Read More](https://www.jungleisland.com/luminosa-chinese-lantern-festival/)

----------------------

![MIAMI ENTERTAINMENT MONTH](https://www.arshtcenter.org/Global/Subscriptions/Broadway%20in%20Miami%202019-20/BIM-1920-Subs-690x310-v3.jpg)

## MIAMI ENTERTAINMENT MONTHS

>**Date:** \
October 1, 2019 - November 30, 2019

October kicks of the two-month long Miami Entertainment Months. You’ll enjoy promotions on everything from craft brewery tastings to movie screenings at state-of-the-art cinemas. Miami’s entertainment scene runs the gamut from concerts to the theater, dance, nightlife and shopping.

[Read More](https://www.miamiandbeaches.com/offers/temptations/miami-entertainment-months)

-----------------------

![Imgur](https://i.imgur.com/kpuTFfg.jpg)

## Miami among top U.S. cities in 2019 for growth

The Magic City outranked most Florida cities on the list, including Orlando and Tampa.

[Read More](https://www.bizjournals.com/southflorida/news/2019/10/15/miami-among-top-us-cities-in-2019-for-growth.html?ana=e_me_set1)

------------------

![Imgur](https://i.imgur.com/FTsfT1Z.jpg)
