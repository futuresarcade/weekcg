---
title: "20190913_wcg"
date: 2019-09-12T10:09:08-04:00
draft: false
tags: ["Coral-Gables","Events"]
categories: ["Events"]
author: "Futuresarcade"
autoCollapseToc: false
contentCopyright: ''
---

<head>
    <style type="text/css">
      table {
        border:2px solid #552448;
        margin-top:10px;
        margin-bottom:50px;
      }
      article {
        clear: left;
      }
      img {
        width: 300px;
      }
    </style>
  </head>
<body>
<table width="700">
<tr valign="middle">
<td rowspan="2" width="220"  bgcolor= "#652f56" style="border-right: #552448 1px solid;">
<img src="https://i.imgur.com/r3dTAc6.png" title="source: imgur.com" width="250" alt="r3dTAc6.png">
</td>
<td rowspan="2" width="10">
</td>
<td>
<font style="color: #552448; font-family: Georgia; font-size: .95em"><strong>Diana Ordonez-Sanchez</strong></font><br />
<font style="color: #939598; font-family: Arial; font-size: .85em">Realtor Associate</font><br />
<font style="color: #939598; font-family: Arial; font-size: .85em">Chairman's Club</font><br />
<font style="color: #939598; font-family: Arial; font-size: .85em"><a href="tel:1-305-510-4698">1-305-510-4698</a> </font><br />
<font style="color: #939598; font-family: Arial; font-size: .85em">Berkshire Hathaway HomeServices EWM Realty | Realty Division</font><br />
</td>
</tr>
<tr>
<td height="40" valign="bottom">
<font style="color: #939598; font-family: Arial; font-size: .85em"><span style="color: #552448;">O:</span> 305-960-2400 |<span style="color: #552448;">E:</span> dianaiordonez@gmail.com | <a href="http://www.dianaordonez.com" style="text-decoration: none; color: #552448;">www.dianaordonez.com</a></font><br />
<font style="color: #939598; font-family: Arial; font-size: .85em">550 S. Dixie Hwy, Coral Gables, Florida 33146</font><br />
</td>
</tr>
</table>

</body>

----------------------------

![Imgur](https://i.imgur.com/sBfDw0V.jpg?1)

## MIAMI SPICE
>**Date:**  \
AUG. 1 - SEPT. 30 \
>**Location:**  \
[Participating Restaurants](https://www.miamiandbeaches.com/offers/temptations/miami-spice-months/list-at-a-glance)

August kicks off two months of Miami Spice, when restaurants in all corners of the destination offer special three-course, prix-fixe menus featuring their tastiest dishes at marked down prices (lunch/brunch $23, dinner $39). Make a reservation, get out there, and taste, taste, taste!

[READ MORE](https://www.miamiandbeaches.com/offers/temptations/miami-spice-month)

------------------

![MATILDA](https://static.wixstatic.com/media/721a33_a0cc8939db594f2a960d6cf1e18e8328~mv2.png/v1/fill/w_1012,h_357,al_c/721a33_a0cc8939db594f2a960d6cf1e18e8328~mv2.png)

## MATILDA

>**Date:** \
Friday 9-13 to 9-29 \
**location:** \
Area Stage Company \
1560 S Dixie Hwy, Coral Gables, FL 33146 \
[MAP](https://goo.gl/maps/ZnuYaYxqXg6WrTH89) \
**Cost:** $15- $45

Roald Dahl's Matilda the Musical is the story of an extraordinary girl who, armed with a vivid imagination, a sharp mind, and a hint of magic, dares to take a stand and change her own destiny. Inspired by the twisted genius of Roald Dahl, book by Dennis Kelly, and original songs by Tim Minchin, this Tony Award-winning musical has won 47 international awards.

[Read More](https://www.areastagecompany.com/matilda)

----------------------------

![Family Day Aragon](http://www.gablescinema.com/media/filmassets/slides/The_Wizard_of_Oz_3.jpg.420x330_q85_crop-smart.jpg)

## FAMILY DAY ARAGON - The Wizard of Oz (80th Anniversary)

>**Date:**  \
Saturday 9-14 and Sunday 9-15, 11:00 am  \
>**Location:** \
Gables Cinema \
260 Aragon Ave \
Coral Gables, FL 33134  \
**Cost:**: $5 \
[MAP](https://goo.gl/maps/A4s1cECHe5H2)

A young Kansas farm girl is swept up by a tornado and taken to the magical land of Oz.

[READ MORE](http://www.gablescinema.com/events/the-wizard-of-oz/)

![Imgur](https://coralgablesmuseum.org/wp-content/uploads/2016/07/Family-Day-7-845x321.jpg)

## FAMILY DAY ARAGON- American Indian Day

>**Date:**  \
Saturday 9-14, 2:00 pm - 5:00 pm \
>**Location:** \
Coral Gables Museum \
285 Aragon Avenue \
Coral Gables, FL 33134 \
>**Cost:** Free \
[MAP](https://www.google.com/maps/place/Coral+Gables+Museum/@25.7505393,-80.2602801,17z/data=!4m5!3m4!1s0x88d9b79a1b3ffaa5:0x9aad47c710d60050!8m2!3d25.7505393!4d-80.2602801)

Let’s honor Native American culture and their important contributions at the Coral Gables Museum for our City Family Day on Aragon event. Children and families will explore the various communities who help make Florida the “Sunshine State,” as well as other American Indians.

[READ MORE](https://coralgablesmuseum.org/event/family-day-on-aragon-celebrate-american-indian-day/)

-------------------

![Biscayne Bay Cruise](https://deeringestate.org/wp-content/uploads/2018/09/Lighthouse-Bay-Cruise-resize-e1538332749879.jpg)

## Lighthouses of Biscayne Bay Cruise

>**Date:**  \
Saturday  9-14, 10:00 am - 2:00 pm \
>**Location:** \
Deering Estate, 16701 SW 72 Ave. Miami FL 33157 \
**Cost:** $$70 per person, plus tax and a processing fee \
[PURCHASE TICKETS](https://www.biscaynenationalparkinstitute.org/sailing-boating/deering-estate-lighthouses-boat-cruise/) \
[MAP](https://goo.gl/maps/dgeiaTG5ZBF2)

Departing from the Deering Estate, this four hour tour will allow you to experience Biscayne National Park in a new light! Guided by a Biscayne National Park Ranger, guests will journey through Biscayne Bay to see the iconic Cape Florida, Fowey Rocks and Boca Chita Lighthouses and learn about the history that surrounds them.

[READ MORE](https://deeringestate.org/event/lighthouse-cruise-september/)

-----------------------



![Frost Music Live](https://s3.amazonaws.com/ovationtix-public-client-assets/prod/client_id_1811/logo_images/FML-Tickets-3.jpg)]

## FROST MUSIC LIVE
Gerard Schwarz’s Debut—Frost Symphony Orchestra

>**Date:** \
Friday 9-14, 7:30 p.m. \
**location:** \
UM Gusman Concert Hall \
1314 Miller Drive \
Coral Gables, FL \
[MAP](https://goo.gl/maps/LBkv9McAYsU1T2fd9) \
**Cost:**$25

Gerard Schwarz, conductor
Aaron Tindall, tuba
In his debut as Frost’s new conductor of the Frost Symphony Orchestra, Gerard Schwarz offers a varied and demanding program.

[Read More](https://ci.ovationtix.com/1811/performance/10445655)

---------------------------


![Bernacle](https://thebarnacle.org/wp-content/uploads/2019/01/row-about-full.jpg)

## BARNACLE UNDER MOONLIGHT CONCERT
THREE SHEETS TO THE WIND

>**Date:** \
Saturday 9-14, 7:00 - 9:00 p.m. \
**location:** \
The Barnacle Historic State Park \
3485 Main Highway \
Coconut Grove FL 33133 \
[MAP](https://goo.gl/maps/4kJ29HW5HksGYzCw6) \
**Cost:**$10)

[Read More](https://www.floridastateparks.org/events/barnacle-under-moonlight-concert-three-sheets-wind)

--------------------

![Farmers Market](https://d36ki7ut62e74z.cloudfront.net/prod/optimized/b170_farmers_market_sign.jpg)


## THE MARKET AT MERRICK

Farmer's Market

>**Date:** \
 Sundays 11 a.m. to 6 p.m. \
>**Location:** \
358 San Lorenzo Ave, Coral Gables, FL 33146 \
[MAP](https://goo.gl/maps/zRXV4KAWXJBouUu68)

Visit our fabulous Farmers.Stop by and pick up some beautiful and tasty produce, healthy homemade products, dessert and more! The Farmers' Market is located between Tourneau and Nordstrom

[READ MORE](https://www.shopsatmerrickpark.com/en/events/the-farmers-market-at-merrick-park-18504.html)

----------------------------


![Metropolis](https://i.imgur.com/bKMfKUX.gif)

## METROPOLIS APARTMENT - FOR SALE

>**Location:** \
9066 SW 73 Ct # 605 Miami, Florida 33156

Loft style apartment at Dadeland. 13 FT ceilings add to the spacious feeling of this open plan 1 bedroom, 1 bath + den. Metropolis Dadeland, walking distance to Restaurants and shopping. 2 swimming pools, Jacuzzi, fitness center, recreational room 24 hr concierge services and valet parking.

[READ MORE](http://yourhomeincoralgables.com/listings/9066-sw-73-ct-605-miami-florida-33156/)
